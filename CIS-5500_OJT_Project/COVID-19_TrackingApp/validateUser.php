<?php
/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Covid Questionnaire Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 *
 * This file stores the Covid questions used to validate a user before they register for service
 *
 */

session_start();
//Allows access to use functions in other files
require_once("Admin/validateForms.php");
require_once("Other/footer.php");

//Calls function to validate the user input
if (isset($_POST['btn-validate-user'])) {
    if (count($_POST) > 0) {


        $userChoice1 = "Yes";
        $userChoice2 = "No";

        $validateQuestion1 = $_POST['question1'];
        $validateQuestion2 = $_POST['question2'];
        $validateQuestion3 = $_POST['question3'];
        $validateQuestion4 = $_POST['question4'];
        $service_ID = $_GET['serviceId'];

   $id = $_SESSION['SESS_ID'];

        if (empty($validateQuestion1) || empty($validateQuestion2)
            || empty($validateQuestion3) || empty ($validateQuestion4)) {
            header("location: ./validateUser.php");
        } else if (($validateQuestion1 === $userChoice1) || ($validateQuestion2 === $userChoice1)
            || ($validateQuestion3 === $userChoice1) || ($validateQuestion4 === $userChoice1)) {
            header("location: ./User/user.php?userAccessId=$id");
        } else if (($validateQuestion1 === $userChoice2) && ($validateQuestion2 === $userChoice2)
            && ($validateQuestion3 === $userChoice2) && ($validateQuestion4 === $userChoice2)) {
            header("location: ./User/registerEvent.php?serviceId=$service_ID");


        }

    }
}

//Checks if the serviceId is set and and assigns it to a variable
if (isset($_GET['serviceId'])) {

//they have an isbn in the url
    $service_Id = $_GET['serviceId'];
   $id = $_SESSION['SESS_ID'];
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Welcome</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/cover/">


    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="css/customStyles.css">
</head>
<body class="text-center" id="main-container">


<style>
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        transition: 0.3s;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
</style>
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">
        <div class="inner">
            <h3 class="masthead-brand text-white">K & J Ministries</h3>

        </div>
    </header>

    <main role="main" class="inner cover">
        <h1 class="cover-heading">Covid-19 Questions</h1>
        <p class="lead"> Please answer the following questions to proceed</p>
    </main>

    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu-welcome" class="col-md-3 bg-info">
                <form method="post" action="">

                    <div class="sidebar-sticky pt-3">
                        <div class="card">
                            <div class="container">
                                <h4>Question 1:</h4>
                                <ul class="nav flex-column text-left">

                                    Are you or any member of your household experiencing any of the following:
                                    <li>a. severe difficulty breathing (e.g struggling for each breath)</li>
                                    <li>b. severe chest pain</li>
                                    <li>c. having a very hard time waking up</li>
                                    <li>d. feeling confused</li>
                                    <li>e. lost consciousness</li>
                                    <li><label for="user-input-yes">Yes</label>
                                        <input type="radio" value="Yes" id="user-input-yes" name="question1"></li>
                                    <li><label for="user-input-no">No</label>
                                        <input type="radio" value="No" id="user-input-no" name="question1"</li>
                                </ul>
                            </div>
                        </div>

                        <br>
                        <br>
                        <div class="card">
                            <div class="container">
                                <h4>Question 2:</h4>
                                <ul class="nav flex-column text-left">

                                    <li class="nav-item">

                                        Are you or any member of your household experiencing any of the following:
                                    <li> a. shortness of breath at rest</li>
                                    <li> b. inability to lie down because of difficulty breathing</li>
                                    <li> c. chronic health conditions that you are having difficulty managing because of
                                        your current resporatory illness
                                    </li>
                                    <li>a. severe difficulty breathing (e.g struggling for each breath)</li>
                                    <li>b. severe chest pain</li>
                                    <li>c. having a very hard time waking up</li>
                                    <li>d. feeling confused</li>
                                    <li>e. lost consciousness</li>
                                    <li><label for="user-input-yes">Yes</label>
                                        <input type="radio" value="Yes" id="user-input-yes" name="question2"></li>
                                    <li><label for="user-input-no">No</label>
                                        <input type="radio" value="No" id="user-input-no" name="question2"</li>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="card">
                            <div class="container">
                                <h4>Question 3:</h4>
                                <ul class="nav flex-column text-left">

                                    <li class="nav-item">

                                        In the past 10 days, have you or any member of your household experienced any of
                                        the following:
                                    <li>a. fever</li>
                                    <li>b. new onset of cough or worsening of chronic cough</li>
                                    <li>c. new or worsening shortness of breath</li>
                                    <li>d. new or worsening difficulty breathing</li>
                                    <li>d. feeling confused</li>
                                    <li> e. sore throat</li>
                                    <li><label for="user-input-yes">Yes</label>
                                        <input type="radio" value="Yes" id="user-input-yes" name="question3"></li>
                                    <li><label for="user-input-no">No</label>
                                        <input type="radio" value="No" id="user-input-no" name="question3"</li>
                                    </li>
                                </ul>
                            </div>
                        </div>


                        <br>
                        <br>
                        <div class="card">
                            <div class="container">
                                <h4>Question 4:</h4>
                                <ul class="nav flex-column text-left">

                                    <li class="nav-item">

                                    <li class="nav-item">
                                        Do you or any member of your household have any of the following:
                                    <li> a. chills</li>
                                    <li>
                                        b. painful swallowing
                                    </li>
                                    <li> c. stuff nose</li>
                                    <li> d. headache</li>
                                    <li>e. muscle or joint ache</li>
                                    <li> f. feeling unwell, fatigue or severe exhaustion</li>
                                    <li>g. nausea, vomitting, diarrhea or unexplained loss of appetite</li>
                                    <li> h. loss of sense of smell or taste</li>
                                    <li>i. conjunctivitis (pink eye)</li>


                                    <li><label for="user-input-yes">Yes</label>
                                        <input type="radio" value="Yes" id="user-input-yes" name="question4"></li>
                                    <li><label for="user-input-no">No</label>
                                        <input type="radio" value="No" id="user-input-no" name="question4"</li>

                                    </li>


                                </ul>
                            </div>
                        </div>
                        <input type="submit" name="btn-validate-user" class="btn-success text-white">


                    </div>

                </form>
            </nav>


            <footer class="mastfoot mt-auto">
                <div class="inner">
                    <?php
                    echo displayFooter();
                    ?>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <script src="dashboard.js"></script>
</body>
</html>



