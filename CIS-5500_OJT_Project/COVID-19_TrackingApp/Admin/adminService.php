<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Admin Service Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */

//Starts session to ensure user is logged in
session_start();


require_once("../Data Access Object/connectDAO.php");
require_once ("../Other/footer.php");
require_once ("../Other/header.php");

//Destroys the session when the user clicks the logout button
if (isset($_POST['btn-logout'])) {
    session_destroy();
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>K & J | Service Registration</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<?php
echo displayNavbar();
?>

<div class="container">

    <div class="py-5 text-center">
        <h4 class="mb-3">Current Services</h4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Service ID</th>
                <th>Service Name</th>
                <th>Service Date</th>
                <th>Service Time</th>
                <th>Service Status</th>
                <th>Service Quantity</th>
                <th>Action</th>
            </tr>
            </thead>


            <?php

            global $mysqli;
            $viewCurrentServices = $mysqli->prepare("SELECT serviceId, service_name, service_date, service_time, service_quantity, 
service_status FROM service ORDER BY serviceId");
            $viewCurrentServices->execute();
            $results = $viewCurrentServices->get_result();

            //Display employee details from the database
            if ($results->num_rows > 0) {
                // output data of each row
                while ($row = $results->fetch_assoc()) {

                    $serviceId = $row["serviceId"];
                    $serviceName = $row["service_name"];
                    $serviceDate = $row["service_date"];
                    $serviceTime = $row["service_time"];
                    $serviceQty = $row["service_quantity"];
                    $serviceStats = $row["service_status"];


                    if ($serviceStats == 'Unavailable') {

                        echo
                        "<tr><td>$serviceId</td>
                        <td>$serviceName</td>
                        <td>$serviceDate</td>
                        <td>$serviceTime</td>
                        <td>$serviceStats</td>
                        <td>$serviceQty</td>
                        <td>Not Available</td>
                    </tr>
                    ";
                    } else {
                        echo
                        "<tr><td>$serviceId</td>
                        <td>$serviceName</td>
                        <td>$serviceDate</td>
                        <td>$serviceTime</td>
                        <td>$serviceStats</td>
                        <td>$serviceQty</td>
                     
                        <td><a href='adminRegisterEvent.php?serviceId=$serviceId'>Register</a></td>
                    </tr>
                    ";
                    }

                }

                //Display a message if there is no records or results
            } else {
                echo "<h1>There is no records to display at this time</h1>";
                exit();
            }
            //Free the memory from the server
            $viewCurrentServices->free_result();

            //Close the database
            $mysqli->close();

            ?>

        </table>


    </div>
<?php
echo displayFooter();

?>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
<script src="form-validation.js"></script>
</body>
</html>