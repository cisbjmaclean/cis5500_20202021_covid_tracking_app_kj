<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Validate Forms Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


function checkServiceForm()
{


    if (isset($_POST['btn-submit'])) {

        if (count($_POST) > 0) {
            global $mysqli;
            // create short variable names
            $service_name = $_POST['service-name'];
            $service_description = $_POST['service-description'];
            $service_date = $_POST['service-date'];
            $service_time = $_POST['service-time'];
            $service_status = $_POST['service-status'];
            $service_quantity = $_POST['service-qty'];

            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($service_name) || empty($service_description) || empty($service_date) || empty($service_time)
                || empty($service_status) || empty($service_quantity)) {

                header("location: createService.php");
                exit();
            } else {


                //sql injection
                $service_name = $mysqli->real_escape_string($service_name);
                $service_description = $mysqli->real_escape_string($service_description);
                $service_date = $mysqli->real_escape_string($service_date);
                $service_time = $mysqli->real_escape_string($service_time);
                $service_status = $mysqli->real_escape_string($service_status);
                $service_quantity = $mysqli->real_escape_string(doubleval($service_quantity));

                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit;
                }

                //Insert input into the books table
                $insertService = "INSERT INTO service (service_name, service_description, service_date, service_time, service_status, service_quantity)
VALUES ('$service_name' , '$service_description' , '$service_date' , '$service_time', '$service_status' , '$service_quantity')";
                ")";



                if (mysqli_query($mysqli, $insertService)) {
                    header("location: adminUser.php");
                    $insertService->free();
                    $mysqli->close();

                } else {
                    header("location: createService.php");
                    $insertService->free();
                    $mysqli->close();
                }


            }


        }
    }

}

/***
 * This function is used to validate the input from the edit service page and redirects them to the Admin page if successful
 */
function editService() {
    if (isset($_POST['submit'])) {

        global $mysqli;

        $id =  $_SESSION['SESS_ID'];
        // create short variable names
        $serviceId = $_POST['serviceId'];
        $service_name = $_POST['service_name'];
        $service_description = $_POST['service_description'];
        $service_date = $_POST['service_date'];
        $service_time = $_POST['service_time'];
        $service_status = $_POST['service_status'];
        $service_quantity = $_POST['service_quantity'];

        if (empty($service_name) || empty($service_description) || empty($service_date) || empty($service_time)
            || empty($service_status) || empty($service_quantity)) {
            echo "You have not entered all the required details.<br />"
                . "Please go back and try again.</body></html>";
            exit;
        }

        require_once("../Data Access Object/connectDAO.php");
        //sql injection
        //sql injection
        $service_name = $mysqli->real_escape_string($service_name);
        $service_description = $mysqli->real_escape_string($service_description);
        $service_date = $mysqli->real_escape_string($service_date);
        $service_time = $mysqli->real_escape_string($service_time);
        $service_status = $mysqli->real_escape_string($service_status);
        $service_quantity = $mysqli->real_escape_string(doubleval($service_quantity));

        // Update query
        $query = "UPDATE service SET service_name='$service_name', service_description='$service_description', service_date='$service_date', service_time='$service_time', service_status='$service_status', service_quantity=$service_quantity WHERE service.serviceId=$serviceId LIMIT 1";
        $result = $mysqli->query($query);

        if ($result) {
            header("location: ../Admin/adminUser.php?userAccessId=$id");
            //Free result
            $result->free();
            //close connection
            $mysqli->close();
        } else {
            header("location: ../Admin/editService.php?userAccessId=$id");
        }
    }
}


function checkGuestForm()
{

    require_once("../Data Access Object/connectDAO.php");

    if (isset($_POST['btn-booking'])) {
        if (count($_POST) > 0) {
            global $mysqli;
            // create short variable names
            $guest_name = $_POST['guest_fullname'];
            $guest_username = $_POST['guest_username'];
            $guest_password = $_POST['guest_password'];
            $guest_new_pass = $_POST['guest_new_password'];
            $guest_number = $_POST['guest_number'];
            $user_roles = $_POST['code-values'];


            //sql injection
            $guest_name = $mysqli->real_escape_string($guest_name);
            $guest_username = $mysqli->real_escape_string($guest_username);
            $guest_password = $mysqli->real_escape_string($guest_password);
            $guest_new_pass = $mysqli->real_escape_string($guest_new_pass);
            $guest_number = $mysqli->real_escape_string($guest_number);
            $user_roles = $mysqli->real_escape_string($user_roles);

            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($guest_name) || empty($guest_username) || empty($guest_password)
                || empty($guest_new_pass) || empty($guest_number) ||empty($user_roles)) {

                header("location: ../UI/createGuest.php");
                exit();

                //Checks if the passwords don't match and redirect the user
            } else if (($guest_password != $guest_new_pass)) {
                header("location: ../UI/createGuest.php");
            } else {


                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit;
                }


                if($user_roles == 'Congregant') {

                    try {
// First of all, let's begin a transaction
                        $mysqli->begin_transaction();
// A set of queries; if one fails, an exception should be thrown
                        $mysqli->query("INSERT INTO useraccess(username, pass_word, fullName, userAccessStatusCode, userTypeCode) 
                               VALUES ('$guest_username', '$guest_new_pass', '$guest_name', 1, 1)");
                        $mysqli->query("INSERT INTO useraccessdetail (phoneNumber)
 VALUES ('$guest_number')");
// If we arrive here, it means that no exception was thrown
// i.e. no query has failed, and we can commit the transaction
                        $mysqli->commit();
                        header("location: ../UI/completeBooking.php");
                        $mysqli->close();
                    } catch (Exception $e) {
// An exception has been thrown
// We must rollback the transaction
                        $mysqli->rollback();
                        header("location: ../UI/createGuest.php");
                        $mysqli->close();
                    }

                } else if ($user_roles == 'Admin') {

                    try {
// First of all, let's begin a transaction
                        $mysqli->begin_transaction();
// A set of queries; if one fails, an exception should be thrown
                        $mysqli->query("INSERT INTO useraccess(username, pass_word, fullName, userAccessStatusCode, userTypeCode) 
                               VALUES ('$guest_username', '$guest_new_pass', '$guest_name', 1, 2)");
                        $mysqli->query("INSERT INTO useraccessdetail (phoneNumber)
 VALUES ('$guest_number')");
// If we arrive here, it means that no exception was thrown
// i.e. no query has failed, and we can commit the transaction
                        $mysqli->commit();
                        header("location: ../UI/completeBooking.php");
                        $mysqli->close();
                    } catch (Exception $e) {
// An exception has been thrown
// We must rollback the transaction
                        $mysqli->rollback();
                        header("location: ../UI/createGuest.php");
                        $mysqli->close();
                    }

                }

            }
        }
    }
}

function checkLoginForm()
{


    require_once("../Data Access Object/connectDAO.php");
    if (isset($_POST['btn-login'])) {
        if (count($_POST) > 0) {
            $userName = $_POST['userName'];
            $userPassword = $_POST['password'];

            if (empty($userName) || empty($userPassword)) {
                header("location: ../User Login/userLogin.php");
                exit();
            }
            global $mysqli;
            $userName = $mysqli->real_escape_string($_POST['userName']);
            $password = $mysqli->real_escape_string($_POST['password']);


            $query = "SELECT username FROM useraccess WHERE username=" . $userName . " AND password('" . $password . "')";

            $output = $mysqli->query($query);

            if ($output = $mysqli->query($query)) {

                if ($output->num_rows == 1) {

                    if ($userName == "ktaylor" && $password == "ktaylor") {
                        header("location:  ../Admin/adminUser.php");
                    } else if ($userName == "akattijnr" && $password == "akattijnr") {
                        header("location: ../User/user");
                    } else {
                        header("location: ../User Login/userLogin.php");
                    }

                }

            }


        }

    }
}

function registerGuest()
{
    require_once("../Data Access Object/connectDAO.php");

    if (isset($_POST['btn-admin-register'])) {
        if (count($_POST) > 0) {
            global $mysqli;

            // create short variable names
            $congregant_name = $_POST['congregant-fullname'];
            $congregant_number = $_POST['congregant-number'];
            $service_name = $_POST['event-name'];
            $service_date = $_POST['event-date'];
            $service_time = $_POST['event-time'];


            $confirmStatus = "Registered";


            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($congregant_name) || empty($congregant_number) || empty($service_name) || empty($service_date)
                || empty($service_time)) {

                header("location: registerGuest.php");

                exit();
            } else {


                //sql injection
                $congregant_name = $mysqli->real_escape_string($congregant_name);
                $congregant_number = $mysqli->real_escape_string($congregant_number);
                $service_name = $mysqli->real_escape_string($service_name);
                $service_date = $mysqli->real_escape_string($service_date);
                $service_time = $mysqli->real_escape_string($service_time);

                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit();
                }

                //Insert input into the books table
                $registerGuest = "INSERT INTO serviceguest (serviceId, serviceName, serviceDate, serviceTime, guestName, guestPhoneNumber, serviceGuestStatusType)
                                       VALUES (2, '$service_name', '$service_date', '$service_time', '$congregant_name', '$congregant_number', '$confirmStatus')";


                //Once insert is successful, the user will see a confirmation message
                if (mysqli_query($mysqli, $registerGuest)) {
                    header("location: ../UI/completeRegistration.php");
                    $mysqli->close();
                    //If it fails to enter the record, the user will be redirected to the Guest Registration page
                } else {
                    header("location: ../UI/registerEvent.php?serviceId=2");
                    //header("location: services.php");

                    $mysqli->close();


                }

            }

        }


    }

}


function validateActiveGuest()
{
    if (isset($_POST['btn-confirm-guest'])) {
        if (count($_POST) > 0) {
            require_once("../Data Access Object/connectDAO.php");
            global $mysqli;


            $activeGuest = $_POST['active-guest'];
            $ID = $_GET['serviceId'];


              if (isset($activeGuest)) {

                  //Insert input into the congregant table
                  $updateActiveGuest = "UPDATE serviceguest SET serviceGuestStatusType = 'Active'
WHERE serviceId ='$ID' ";

                  //Once insert is successful, the user will see a confirmation message
                  if (mysqli_query($mysqli, $updateActiveGuest)) {
                      header("location: ../UI/completeValidation.php");
                      $mysqli->close();
                      //If it fails to enter the record, the user will be redirected to the Guest Registration page
                  } else {
                      header("location: ../UI/validateGuest.php");
                      $mysqli->close();
                  }


              }
                  if (!isset($activeGuest)) {
                      //Insert input into the congregant table
                      $updateNonActiveGuest = "UPDATE serviceguest SET serviceGuestStatusType = 'Not Active'
WHERE serviceId ='$ID' ";

                      //Once insert is successful, the user will see a confirmation message
                      if (mysqli_query($mysqli, $updateNonActiveGuest)) {
                          header("location: ../UI/completeValidation.php");
                          $mysqli->close();
                          //If it fails to enter the record, the user will be redirected to the Guest Registration page
                      } else {
                          header("location: ../UI/validateGuest.php");
                          $mysqli->close();
                      }


                  }


        } else {

            header("location: ../UI/validateGuest.php");
        }
    }
}


/**
 *
 * This function is used to validate user input for the Covid questions
 * and redirect them accordingly
 */
function validateUserQuestions()
{


    if (isset($_POST['btn-validate-user'])) {
        if (count($_POST) > 0) {


            $userChoice1 = "Yes";
            $userChoice2 = "No";

            $validateQuestion1 = $_POST['question1'];
            $validateQuestion2 = $_POST['question2'];
            $validateQuestion3 = $_POST['question3'];
            $validateQuestion4 = $_POST['question4'];
            $service_ID = $_GET['serviceId'];
if(isset($_GET['serviceId'])) {
    $userId = $_SESSION['SESS_ID'];
}
            if (empty($validateQuestion1) || empty($validateQuestion2)
                || empty($validateQuestion3) || empty ($validateQuestion4)) {
                header("location: ./validateUser.php");
            } else if (($validateQuestion1 === $userChoice1) || ($validateQuestion2 === $userChoice1)
                || ($validateQuestion3 === $userChoice1) || ($validateQuestion4 === $userChoice1)) {
                header("location: ./User/user.php?userAccessId=$userId");
            } else if (($validateQuestion1 === $userChoice2) && ($validateQuestion2 === $userChoice2)
                && ($validateQuestion3 === $userChoice2) && ($validateQuestion4 === $userChoice2)) {
                header("location: ./User/registerEvent.php?serviceId=$service_ID");


            }

        }
    }

}




function confirmAdminRegistration()
{

    require_once("../Data Access Object/connectDAO.php");

    if (isset($_POST['btn-register'])) {
        if (count($_POST) > 0) {
            global $mysqli;

            // create short variable names
            $congregant_name = $_POST['congregant-fullname'];
            $congregant_number = $_POST['congregant-number'];
            $service_name = $_POST['current-service'];
            $service_date = $_POST['service-date'];
            $service_time = $_POST['service-time'];
            $service_ID = $_GET['serviceId'];
            $confirmStatus = "Registered";


            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($congregant_name) || empty($congregant_number) || empty($service_name) || empty($service_date)
                || empty($service_time)) {

                header("location: registerEvent.php?serviceId=$service_ID");
                //header("location: registerEvent.php");

                exit();
            } else {


                //sql injection
                $congregant_name = $mysqli->real_escape_string($congregant_name);
                $congregant_number = $mysqli->real_escape_string($congregant_number);
                $service_name = $mysqli->real_escape_string($service_name);
                $service_date = $mysqli->real_escape_string($service_date);
                $service_time = $mysqli->real_escape_string($service_time);

                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit();
                }

                //Insert input into the books table
                $registerGuest = "INSERT INTO serviceguest (serviceId, serviceName, serviceDate, serviceTime, guestName, guestPhoneNumber, serviceGuestStatusType)
                                       VALUES ('$service_ID', '$service_name', '$service_date', '$service_time', '$congregant_name', '$congregant_number', '$confirmStatus')";


                //Once insert is successful, the user will see a confirmation message
                if (mysqli_query($mysqli, $registerGuest)) {
                    header("location: ../UI/completeRegistration.php?serviceId=$service_ID");
                    $mysqli->close();
                    //If it fails to enter the record, the user will be redirected to the Guest Registration page
                } else {
                    //header("location: ../UI/registerEvent.php?serviceId=$service_ID");
                    header("location: services.php");

                    $mysqli->close();


                }

            }

        }


    }
}

function confirmUserRegistration()
{

    require_once("../Data Access Object/connectDAO.php");

    if (isset($_POST['btn-user-register'])) {
        if (count($_POST) > 0) {
            global $mysqli;

            // create short variable names
            $congregant_name = $_POST['congregant-fullname'];
            $congregant_number = $_POST['congregant-number'];
            $service_name = $_POST['current-service'];
            $service_date = $_POST['service-date'];
            $service_time = $_POST['service-time'];
            $service_ID = $_GET['serviceId'];
            $confirmStatus = "Registered";


            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($congregant_name) || empty($congregant_number) || empty($service_name) || empty($service_date)
                || empty($service_time)) {

                header("location: registerEvent.php?serviceId=$service_ID");
                //header("location: registerEvent.php");

                exit();
            } else {


                //sql injection
                $congregant_name = $mysqli->real_escape_string($congregant_name);
                $congregant_number = $mysqli->real_escape_string($congregant_number);
                $service_name = $mysqli->real_escape_string($service_name);
                $service_date = $mysqli->real_escape_string($service_date);
                $service_time = $mysqli->real_escape_string($service_time);

                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit();
                }

                //Insert input into the books table
                $registerGuest = "INSERT INTO serviceguest (serviceId, serviceName, serviceDate, serviceTime, guestName, guestPhoneNumber, serviceGuestStatusType)
                                       VALUES ('$service_ID', '$service_name', '$service_date', '$service_time', '$congregant_name', '$congregant_number', '$confirmStatus')";


                //Once insert is successful, the user will see a confirmation message
                if (mysqli_query($mysqli, $registerGuest)) {
                    header("location: ../UI/completeRegistration.php?serviceId=$service_ID");
                    $mysqli->close();
                    //If it fails to enter the record, the user will be redirected to the Guest Registration page
                } else {
                    //header("location: ../UI/registerEvent.php?serviceId=$service_ID");
                    header("location: services.php");

                    $mysqli->close();


                }

            }

        }


    }
}

function updateUserInfo()
{

    require_once("../Data Access Object/connectDAO.php");

    if (isset($_POST['btn-user'])) {
        if (count($_POST) > 0) {
            global $mysqli;

            // create short variable names
            $fullName = $_POST['user-fullname'];
            $userName = $_POST['user-username'];
            $passWord = $_POST['user-password'];
            $userId = $_GET['userAccessId'];
            $user_phone = $_POST['user-number'];


            //If fields are empty, show user an appropriate error message and send them back to add new book
            if (empty($fullName) || empty($userName) || empty($passWord)) {

                header("location: editUser.php?userAccessId=$userId");

                exit();
            } else {


                //sql injection
                $fullName = $mysqli->real_escape_string($fullName);
                $userName = $mysqli->real_escape_string($userName);
                $passWord = $mysqli->real_escape_string($passWord);

                //Show message if no database connection is established.
                if (mysqli_connect_errno()) {
                    echo "Error: Could not connect to database.  Please try again later.";
                    exit();
                }


                try {
// First of all, let's begin a transaction
                    $mysqli->begin_transaction();
// A set of queries; if one fails, an exception should be thrown
                    $mysqli->query("UPDATE  useraccess SET username = '$userName', pass_word = '$passWord', fullName = '$fullName'
WHERE useraccessId = '$userId'");
                    $mysqli->query("UPDATE useraccessdetail
SET phoneNumber = '$user_phone' 
WHERE useraccessId = '$userId'");
// If we arrive here, it means that no exception was thrown
// i.e. no query has failed, and we can commit the transaction
                    $mysqli->commit();
                    header("location: viewUsers.php");
                    $mysqli->close();
                } catch (Exception $e) {
// An exception has been thrown
// We must rollback the transaction
                    $mysqli->rollback();
                    header("location: editUser.php?userAccessId=$userId");
                    $mysqli->close();
                }

            }

        }


    }

}


function testUpdate()
{

    require_once("../Data Access Object/connectDAO.php");


    global $mysqli;


    $service_ID = $_GET['serviceId'];
    //Insert input into the books table
    $updateServiceQty = "DROP TRIGGER IF EXISTS `update_service_qty`;CREATE DEFINER=`root`@`localhost` TRIGGER `update_service_qty` AFTER INSERT ON `serviceguest` FOR EACH ROW UPDATE service 
JOIN serviceguest
ON service.serviceId = serviceguest.serviceId
SET service_quantity =service_quantity - 1 
WHERE $service_ID = serviceguest.serviceId";


    //Once insert is successful, the user will see a confirmation message
    if (mysqli_query($mysqli, $updateServiceQty)) {
        $mysqli->close();
        //If it fails to enter the record, the user will be redirected to the Guest Registration page
    } else {
        //header("location: ../UI/registerEvent.php?serviceId=$service_ID");
        header("location: creeateGuest.php");

        $mysqli->close();


    }

}



