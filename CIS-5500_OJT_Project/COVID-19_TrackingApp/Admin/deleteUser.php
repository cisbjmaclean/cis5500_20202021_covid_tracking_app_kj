<?php
// Author: Elikem K. Akatti Junior
// Purpose: Delete book page
// Date: November 27, 2020


session_start();


// set up connection
require_once("../Data Access Object/connectDAO.php");
global $mysqli;

$userAccessId = "";
$msg = "";
// Process delete operation after confirmation
if (isset($_GET["userAccessId"]) && !empty($_GET["userAccessId"])) {
    //Sanitize the parameter
    $userAccessId = $mysqli->real_escape_string($_GET['userAccessId']);
    // example UPDATE query
    $query = "DELETE FROM useraccess WHERE useraccess.userAccessId =$userAccessId ";
    $result = $mysqli->query($query);

    if ($result) {
        $msg = "User deleted successfully. " . $mysqli->affected_rows . " User deleted from database. <a href='viewUsers.php'>View all Users</a>";

    } else {
        $msg = "Error deleting user: " . $mysqli->error;
    }

    $mysqli->close();

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <div class="navbar-header navbar-right">
                <a class="navbar-brand" href="../logout.php">Logout</a>
            </div>
        </div>
    </nav>

    <h2>K & J Ministries</h2>
    <p class="error"><?php echo $msg ?></p>
</div>
</body>
</html>
