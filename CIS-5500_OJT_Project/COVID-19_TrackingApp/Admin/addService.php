<?php
session_start();


?>
<!doctype html>
<html lang="en">
<head>
    <title>K & J Ministries - Add New Service</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
</head>

<body>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.html">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../index.html"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <h4>Guests:</h4>
                        <a class="nav-link active" href="../UI/bookService.php">
                            <img src="../COVID-19%20Tracking%20Media/img/user-image-with-black-background.png">

                            Create New Guest
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../UI/viewGuest.php">
                            <img src="../COVID-19%20Tracking%20Media/img/view-details.png">
                            View Guest
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../UI/validateGuest.php">
                            <img src="../COVID-19%20Tracking%20Media/img/approved-signal.png">

                            Validate Guest
                        </a>
                    </li>
                    <h4>Services:</h4>
                    <li class="nav-item">
                        <a class="nav-link" href="createService.php">
                            <img src="../COVID-19%20Tracking%20Media/img/edit.png">
                            Create Service
                        </a>
                    </li>
                    <h4>User:</h4>
                    <li class="nav-item">
                        <a class="nav-link" href="createService.php">
                            <img src="../COVID-19%20Tracking%20Media/img/user-image-with-black-background.png">
                            Create New User
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="editUser.php">
                            <img src="../COVID-19%20Tracking%20Media/img/draw.png">
                            Edit User Account
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="viewUsers.php">
                            <img src="../COVID-19%20Tracking%20Media/img/view-details.png">
                            View User Account
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="deleteUser.php">
                            <img src="../COVID-19%20Tracking%20Media/img/trash-bin.png">
                            Delete New User
                        </a>
                    </li>
                    </li>
                </ul>
            </div>
        </nav>
<div id="container">


    <?php

    if (isset($_POST['submit'])) {

    // create short variable names
    $service_name = $_POST['service_name'];
    $service_description = $_POST['service_description'];
    $service_date = $_POST['service_date'];
    $service_time = $_POST['service_time'];
    $service_status = $_POST['service_status'];
    $service_quantity = $_POST['service_quantity'];

    //If fields are empty, show User an appropriate error message and send them back to add new service
    if (empty($service_name) || empty($service_description) || empty($service_date) || empty($service_time)
        || empty($service_status) || empty($service_quantity)) {

        echo "Please fill all required space.";
        echo "An error has occurred.  The item was not added. <a href='createService.php'>Try again?</a>";
        exit();
    }
    //Create DB object
    require_once ("../Data Access Object/connectDAO.php");

    //sql injection
    $service_name = $mysqli->real_escape_string($service_name);
    $service_description = $mysqli->real_escape_string($service_description);
    $service_date = $mysqli->real_escape_string($service_date);
    $service_time = $mysqli->real_escape_string($service_time);
    $service_status = $mysqli->real_escape_string($service_status);
    $service_quantity = $mysqli->real_escape_string($service_quantity);

    //Show message if no database connection is established.
    if (mysqli_connect_errno()) {
        echo "Error: Could not connect to database.  Please try again later.";
        exit;
    }

    //Insert input into the books table
    $query = "INSERT INTO service VALUES (NULL,'" . $service_name . "', '" . $service_description . "', '" . $service_date . "', '" . $service_time . "', '" . $service_status . "',  '". $service_quantity ."')";

    $result = $mysqli->query($query);

    if ($result) {
    echo $mysqli->affected_rows . " service inserted into database. <a href='createService.php'>Add another Service?</a>";

    //Display book inventory
    $query = "SELECT * FROM service ORDER BY serviceId";

    // Here we use our $mysqli object created above and run the query() method. We pass it our query from above.
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    echo "<table class='table table-bordered table-striped'>";
    echo "<thead>";
    if ($num_results > 0) {
        //  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the books retrieved with the query
        $service = $result->fetch_all(MYSQLI_ASSOC);
    }
    ?>
    <table class="table table-bordered">
        <tr>
            <th>Service ID</th><th>Service Name</th><th>Service Description</th><th>Service Date</th><th>Service Time</th><th>Service Status</th><th>Service Quantity</th><th>Action</th>
        </tr>
        <?php

        echo "<tbody>";

        foreach ($service as $s) {
            echo "<tr>";
            $i = 0;

            foreach ($s as $k => $v) {

                if ($k == 'serviceId') {
                    echo "<td>" . $v . "</td>";
                    $serviceId = $v;
                } else {
                    echo "<td>" . $v . "</td>";
                }

                if (($i == count($s) - 1)) {
                    echo "<td>";
                    echo "<div class='btn-toolbar'>";
                    echo "<a href='editService.php?serviceId=" . $serviceId . "' title='Add Service' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit Service</a>";
                    echo "<a href='deleteService.php?serviceId=" . $serviceId . "' title='Delete Service' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete Service</a>";
                    echo "</div>";
                    echo "</td>";
                }

                $i++;
            }
            echo "</tr>";
        }

        echo "</td></tr>";

        echo "</tbody>";
        echo "</table>";

        $result->free();
        $mysqli->close();
        } else {
            echo "An error has occurred.  The item was not added. <a href='createService.php'>Try again?</a>";
        }

        } else {
            //If couldn't add a new book, show error message
            header("location:createService.php?error=noform");
            exit();
        }

        ?>

</div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script>
</body>
</html>
