<?php
require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");

validateActiveGuest();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>K & J Ministries | Validate Guests</title>



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
          rel="stylesheet" type="text/css"/>
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="css/customStyles.css" rel="stylesheet"/>

</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="adminUser.php"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>
<h3>Events</h3>
<p>The table below displays all active events and congregants</p>

<form method="post" action="">
    <table class="table table-striped table-hover style='text-align: center">
        <thead>
        <tr>
            <th>Service ID</th>
            <th>Guest Name</th>
            <th>Guest Phone Number</th>
            <th>Service Name</th>
            <th>Service Date</th>
            <th>Service Time</th>
            <th>Guest Status</th>
            <th>Active</th>
        </tr>
        </thead>


        <?php

        if (isset($_GET['serviceId'])) {



                global $mysqli;
                $iD = $_GET['serviceId'];


                $viewCurrentEvents = $mysqli->prepare("SELECT * FROM serviceguest WHERE serviceId= '$iD' AND serviceGuestStatusType='Registered' 
ORDER BY id");
                $viewCurrentEvents->execute();
                $output = $viewCurrentEvents->get_result();

                //Display employee details from the database
                if ($output->num_rows > 0) {
                    // output data of each row
                    while ($row = $output->fetch_assoc()) {


                        $serviceID = $row["serviceId"];
                        $service_Name = $row["serviceName"];
                        $service_Date = $row["serviceDate"];
                        $service_Time = $row["serviceTime"];
                        $guestFullName = $row["guestName"];
                        $guestPhoneNum = $row["guestPhoneNumber"];
                        $serviceStatusType = $row["serviceGuestStatusType"];


                        echo
                        "<tr><td name='service_id'>$serviceID</td>
                        <td>$guestFullName</td>
                        <td>$guestPhoneNum</td>
                        <td>$service_Name</td>
                        <td>$service_Date</td>
                        <td>$service_Time</td>
                       <td>$serviceStatusType</td>
                     
                        <td><input type='checkbox' name ='active-guest' value='Yes'>Yes</td>
                    
                       
                     
                    </tr>
                    ";

                    }

                    //Display a message if there is no records or results
                } else {
                    echo "<h4> There is no records to display at this time</h4>";
                    exit();
                }
                echo "</table>
            </div>
           ";
                //Free the memory from the server
                $viewCurrentEvents->free_result();

                //Close the database
                $mysqli->close();


        }
        echo "
    <button type='submit' class='btn btn-outline-success' name='btn-confirm-guest'>Confirm Guest</button>

</form>";
        ?>











        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Third party plugin JS-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
</body>
</html>









<!-- Bootstrap core JS-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>
</html>

