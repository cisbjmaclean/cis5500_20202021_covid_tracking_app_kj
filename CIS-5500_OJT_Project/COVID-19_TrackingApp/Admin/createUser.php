<?php


/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Create User Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


session_start();

require_once("../Data Access Object/connectDAO.php");
require_once("validateForms.php");
require_once("../Other/header.php");
checkGuestForm();




?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!--Navigation-->
<?php
echo displayNavbar();
?>
<br>
<br>
<br>
<div id="container">
    <div class="album py-5 bg-light">
        <div class="form-row">
            <form action="" method="post">

                <div class="col-md-4">
                    <h2 class="customer-heading"> Guest Information</h2>
                    <div class="col-md-4">
                        <input type="text" id="fullName" class="form-control" placeholder="Full Name"
                               name="guest_fullname">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="tel" id="phoneNumber" class="form-control"
                               placeholder="Phone Number (902-555-5555)"
                               name="guest_number">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <label id="user-types-label" for="user-type">User Types:</label>
                        <select name="code-values" id="user-type" class="custom-select">
                            <option>Select</option>
                            <?php
                            global $mysqli;
                            $getUserTypes = $mysqli->prepare("SELECT codeTypeId, codeValueSequence, englishDescription FROM codevalue
WHERE codeTypeId = 1 AND codeValueSequence = 1 OR codeTypeId = 1 AND codeValueSequence = 2 ");
                            $getUserTypes->execute();
                            $results = $getUserTypes->get_result();

                            //Display employee details from the database
                            if ($results->num_rows > 0) {
                                // output data of each row
                                while ($row = $results->fetch_assoc()) {

                                    $userTypes = $row["englishDescription"];

                                    echo "
                          
<option>$userTypes</option>

";


                                }


                                //Display a message if there is no records or results
                            } else {
                                echo "<h1>There is no records to display at this time</h1>";
                                exit();
                            }

                            //Free the memory from the server
                            $getUserTypes->free_result();
                            $mysqli->close();
                            ?>
                        </select>
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="text" id="user-Name" class="form-control" placeholder="Username"
                               name="guest_username">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="password" id="password" class="form-control" placeholder="New Password"
                               name="guest_password">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="password" id="new-password" class="form-control" placeholder="Confirm Password"
                               name="guest_new_password">
                    </div>
                    <br>

                </div>


        </div>
        <button type="submit" id="btn-guest-booking" class="btn btn-outline-primary" name="btn-booking">Submit</button>


        </form>
    </div>

</div>
</div>
</body>
</html>

