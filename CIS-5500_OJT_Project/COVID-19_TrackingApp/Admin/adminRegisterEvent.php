<?php
/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Admin Register User Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 *
 * This file stores the user controls for the Admin to register a new guest for a service
 *
 */

//Start the session
session_start();

//Allows access to use functions and variables in other files
require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");
require_once("../Other/header.php");

//Calls function to validate the user input
confirmAdminRegistration();

//Prevents user from accessing this page unless answered the Covid questions
if (!isset($_SERVER['HTTP_REFERER'])) {
    header("location: ../validateUser.php");
}


//Capture service id from url
if (isset($_GET['serviceId'])) {

    $service_Id = $_GET['serviceId'];

    //Database variable
    global $mysqli;


    //Removes special characters
    $service_Id = $mysqli->real_escape_string($service_Id);


    $query = "SELECT * FROM service WHERE service.serviceId = '$service_Id'";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Service not found.";
    } else {
        $row = $result->fetch_assoc();
        $service_name = $row['service_name'];
        $service_description = $row['service_description'];
        $service_date = $row['service_date'];
        $service_time = $row['service_time'];
        $service_status = $row['service_status'];
        $service_quantity = $row['service_quantity'];

    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<?php
echo displayNavbar();
?>
<div id="container">
    <div class="album py-5 bg-light">
        <div class="form-row">
            <form action="" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="customer-heading">Service Registration</h2>
                        <div class="col-md-4">
                            <input type="text" id="congregant_name" class="form-control" placeholder="Full Name"
                                   name="congregant-fullname">
                        </div>
                        <br>
                        <br>

                        <div class="col-md-4">
                            <input type="tel" id="congregant_phone" class="form-control"
                                   placeholder="Phone Number (902-555-5555)"
                                   name="congregant-number">
                        </div>

                        <div class="col-md-4" id="service-div">
                            <label id="register-service-name" for="service-type">Current Services:</label>
                            <select name="current-service" id="service-type" class="custom-select">
                                <option><?php echo $service_name; ?></option>

                            </select>
                        </div>

                        <div class="col-md-4">
                            <label id="register-service-date" for="service-date">Services Date:</label>
                            <select name="service-date" id="service-date" class="custom-select">
                                <option><?php echo $service_date; ?></option>

                            </select>

                        </div>


                        <div class="col-md-4">
                            <label id="register-service-time" for="service-qty">Service Times:</label>
                            <select name="service-time" id="service-time" class="custom-select">
                                <option><?php echo $service_time; ?></option>
                            </select>

                        </div>


                    </div>
                </div>
                <br>

                <br>
                <button type="submit" id="btn-register-event" class="btn btn-outline-info" name="btn-register">
                    Register
                </button>


        </div>

        </form>
    </div>
</div>

</body>
</html>
