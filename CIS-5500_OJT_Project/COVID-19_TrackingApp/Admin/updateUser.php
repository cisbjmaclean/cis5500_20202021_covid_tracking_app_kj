<?php

// Author: Elikem K. Akatti Junior
// Purpose: Update page that update s data
// Date: March 27, 2021

?>
<!doctype html>
<html lang="en">
<head>
    <title>K & J - Update</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <div class="navbar-header navbar-right">
                <a class="navbar-brand" href="../logout.php">Logout</a>
            </div>
        </div>
    </nav>
    <h1>K & J Ministries</h1>
    <?php
    if(isset($_POST['submit'])) {
        // create short variable names
        $userAccessId = $_POST['userAccessId'];
        $fullName =$_POST['fullName'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $userAccessStatusCode = $_POST['userAccessStatusCode'];
        $userTypeCode = $_POST['userTypeCode'];


        //if variables are empty, show appropriate message and exit
        if (empty($fullName) || empty($username) || empty($password) || empty($userAccessStatusCode) || empty($userTypeCode)) {
            echo "You have not entered all the required details.<br />"
                . "Please go back and try again.</body></html>";
            exit;
        }

        require_once ("../Data Access Object/connectDAO.php");

        //sql injection
        $fullName = $mysqli->real_escape_string($fullName);
        $username = $mysqli->real_escape_string($username);
        $password = $mysqli->real_escape_string($password);
        $userAccessStatusCode = $mysqli->real_escape_string($userAccessStatusCode);
        $userTypeCode = $mysqli->real_escape_string(doubleval($userTypeCode));

        // Update query
        $query = "UPDATE useraccess SET fullName='$fullName', password='$password', userAccessStatusCode='$userAccessStatusCode', userTypeCode='$userTypeCode' WHERE useraccess.userAccessId=$userAccessId LIMIT 1";
        $result = $mysqli->query($query);

        //echo $query;

        if ($result) {
            echo $mysqli->affected_rows . " User updated in database. <a href='viewUsers.php'>View all Users</a>";

            //select s with this id
            $query = "SELECT *  FROM `useraccess`  where useraccess.userAccessId=$userAccessId";


            //Here we use our $db object created above and run the query() method. We pass it our query from above.
            $result = $mysqli->query($query);

            // Here we 'get' the num_rows attribute of our $result object - this is key to knowing if we should show the results or
            // display an error message, or perhaps just to say we don't have any results.
            $num_results = $result->num_rows;

            if ($num_results > 0) {
                //  $result->fetch_all(MYSQLI_ASSOC) returns a numeric array of all the service retrieved with the query
                $useraccess = $result->fetch_all(MYSQLI_ASSOC);

                echo "<table class='table table-bordered'><tr>";
                //This dynamically retieves header names
                foreach ($useraccess[0] as $k => $v) {
                    echo "<th>" . $k . "</th>";
                }

                echo "<th>Action</th>";

                echo "</tr></thead>";
                echo "<tbody>";
                //Create a new row for each s
                foreach ($useraccess as $s) {
                    echo "<tr>";
                    $i = 0;

                    foreach ($s as $k => $v) {

                        if ($k == 'userAccessId') {
                            echo "<td>" . $v . "</td>";
                            $userAccessId = $v;
                        } else {
                            echo "<td>" . $v . "</td>";
                        }
                        if (($i == count($s) - 1)) {
                            echo "<td>";
                            echo "<div class='btn-toolbar'>";
                            echo "<a href='editUser.php?userAccessId=" . $userAccessId . "' title='Edit Service' class='btn btn-info btn-xs' data-toggle='tooltip'>Edit</a>";
                            echo "<a href='deleteUser.php?userAccessId=" . $userAccessId . "' title='Delete Service' class='btn btn-info btn-xs' data-toggle='tooltip'>Delete</a>";
                            echo "</div>";
                            echo "</td>";
                        }
                        $i++;
                    }
                    echo "</tr>";

                }

                echo "<tr><td colspan='8'>";
                echo "<a href='createService.php' title='View Record' class='btn btn-info' data-toggle='tooltip'>Add a New User</a>";
                echo "</td></tr>";

                echo "</tbody>";
                echo "</table>";
            }
            //Free result
            $result->free();
            //close connection
            $mysqli->close();
        } else {
            //Show appropriate message if update not successful
            echo "An error has occurred.  The item was not updated.";
        }
    }else{
        //Redirct to view users page
        header("location: viewUsers.php");
        exit();
    }
    ?>
</div>
</body>
</html>