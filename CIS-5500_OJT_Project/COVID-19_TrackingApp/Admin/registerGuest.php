<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Admin Home Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


session_start();

require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");

registerGuest();

//Prevents user from accessing this page unless answered the Covid questions
if (!isset($_SERVER['HTTP_REFERER'])) {
    header("location: ../validateUser.php");
}


// extract the GET variable isbn
if (isset($_GET['serviceId'])) {

//they have an isbn in the url
    $service_Id = $_GET['serviceId'];

    global $mysqli;

    $service_Id = $mysqli->real_escape_string($service_Id);

    // get the data for just the book we want to edit!
    $query = "SELECT * FROM service WHERE service.serviceId = '$service_Id'";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Service not found.";
    } else {
        $row = $result->fetch_assoc();
        $service_name = $row['service_name'];
        $service_description = $row['service_description'];
        $service_date = $row['service_date'];
        $service_time = $row['service_time'];
        $service_status = $row['service_status'];
        $service_quantity = $row['service_quantity'];

    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>


<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="adminUser.php"><img
                                src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img
                                src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>
<div id="container">
    <div class="album py-5 bg-light">
        <div class="form-row">
            <form action="" method="post">


                <div class="col-md-4">
                    <h2 class="customer-heading">Service Registration</h2>
                    <div class="col-md-4">
                        <input type="text" id="guestFullName" class="form-control" placeholder="Full Name"
                               name="congregant-fullname">
                    </div>
                    <br>

                    <br>
                    <div class="col-md-4">
                        <input type="tel" id="guestPhoneNumber" class="form-control"
                               placeholder="Phone Number (902-555-5555)"
                               name="congregant-number">
                    </div>

                    <div class="col-md-4" id="service-div">
                        <label id="currentService" for="service-type">Current Services:</label>
                        <select id="guestServices" name="event-name" class="form-control">
                            <option>Select</option>
                            <?php
                            global $mysqli;

                            $displayServices = $mysqli->prepare("SELECT serviceId, service_name FROM service");
                            $displayServices->execute();
                            $output = $displayServices->get_result();

                            //Display employee details from the database
                            if ($output->num_rows > 0) {
                                // output data of each row
                                while ($row = $output->fetch_assoc()) {
                                    $serviceName = $row["service_name"];
                                    $service_number = $row["serviceId"];


                                    echo "<option>$serviceName</option>";


                                }

                                //Display a message if there is no records or results
                            } else {
                                echo "<option>There is no services to display at this time</option>";
                                exit();

                            }
                            //Free the memory from the server
                            $displayServices->free_result();


                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <!-- Price input-->
                        <label id="guestServiceDate" for="service_status">Service Date</label>
                        <select id="serviceDate" name="event-date" class="form-control">
                            <option>Select</option>
                            <?php
                            global $mysqli;

                            $displayServices = $mysqli->prepare("SELECT service_date FROM service ");
                            $displayServices->execute();
                            $output = $displayServices->get_result();

                            //Display employee details from the database
                            if ($output->num_rows > 0) {
                                // output data of each row
                                while ($row = $output->fetch_assoc()) {
                                    $serviceDate = $row["service_date"];
                                    echo "<option>$serviceDate</option>";


                                }

                                //Display a message if there is no records or results
                            } else {
                                echo "<option>There is no services to display at this time</option>";
                                exit();

                            }
                            //Free the memory from the server
                            $displayServices->free_result();


                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <!-- Price input-->
                        <label id="serviceTime" for="service_status">Service Time</label>
                        <select id="event-Time" name="event-time" class="form-control">
                            <option>Select</option>
                            <?php
                            global $mysqli;

                            $displayServices = $mysqli->prepare("SELECT service_time FROM service  ");
                            $displayServices->execute();
                            $output = $displayServices->get_result();

                            //Display employee details from the database
                            if ($output->num_rows > 0) {
                                // output data of each row
                                while ($row = $output->fetch_assoc()) {
                                    $serviceTime = $row["service_time"];
                                    echo "<option>$serviceTime</option>";


                                }

                                //Display a message if there is no records or results
                            } else {
                                echo "<option>There is no services to display at this time</option>";
                                exit();

                            }
                            //Free the memory from the server
                            $displayServices->free_result();


                            $mysqli->close();


                            ?>
                        </select>


                    </div>
                </div>
                <br>

                <br>
                <button type="submit" id="btn-confirm-service" class="btn btn-outline-primary"
                        name="btn-admin-register">Register
                </button>

            </form>
        </div>

    </div>
</div>


</body>
</html>
