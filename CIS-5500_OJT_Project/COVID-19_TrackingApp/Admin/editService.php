<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Edit Service Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


session_start();
require_once ("../Other/header.php");
require_once ("../Admin/validateForms.php");

editService();

// extract the GET variable isbn
if (isset($_GET['serviceId'])) {

    //they have an isbn in the url
    $serviceId = $_GET['serviceId'];
    $id =  $_SESSION['SESS_ID'];
    // connect to db
    require_once("../Data Access Object/connectDAO.php");
    global $mysqli;

    $serviceId = $mysqli->real_escape_string($serviceId);

    // get the data for just the book we want to edit!
    $query = "SELECT * FROM service WHERE service.serviceId = '$serviceId'";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Service not found.";
    } else {
        $row = $result->fetch_assoc();
        $service_name = $row['service_name'];
        $service_description = $row['service_description'];
        $service_date = $row['service_date'];
        $service_time = $row['service_time'];
        $service_status = $row['service_status'];
        $service_quantity = $row['service_quantity'];

    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}










?>
<!doctype html>
<html lang="en">
<head>
    <title>K & J | Edit Service</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

    <link href="../css/customStyles.css" rel="stylesheet"/>
</head>
<body>
<div id="container">
    <?php


       echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?userAccessId=$id'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";


    ?>
    <!-- <p><a href="newBook.php">Add a new book</a> - <a href="inventory.php">View all Books</a></p>-->

    <h1>K & J - Edit Service Entry</h1>
    <?php
    // if message gets set above it means there is a problem and we don't have a book with that id to edit or it isn't provided
    if (isset($message)) {
        echo $message;
    } else {
    // we have all we need so let's display the book
    ?>

    <div class="newBook-form">
        <form action="" method="post">
            <fieldset class="scheduler-border">
                <div class="form-group">
                    <!-- ISBN input-->
                    <label for="service_name">Service Name:</label>
                    <input type="text" class="form-control" id="service_name" value='<?php echo $service_name ?>'
                           placeholder="Enter Service Name" name="service_name">
                </div>
                <div class="form-group">
                    <!-- Author input-->
                    <label for="$service_description">Service Description:</label>
                    <input type="text" class="form-control" id="$service_description"
                           value='<?php echo $service_description ?>' placeholder="Enter Service Description"
                           name="service_description">
                </div>
                <div class="form-group">
                    <!-- Title input-->
                    <label for="service_date">Service Date:</label>
                    <input type="date" class="form-control" id="service_date" value='<?php echo $service_date ?>'
                           placeholder="Enter Service Date" name="service_date">
                </div>
                <div class="form-group">
                    <!-- Price input-->
                    <label for="service_time">Service Time</label>
                    <input type="time" class="form-control" id="service_time" value='<?php echo $service_time ?>'
                           placeholder="Enter Service Time" name="service_time">
                </div>
                <div class="form-group">
                    <!-- Price input-->
                    <label for="service_status">Service Status</label>
                    <select name="service_status" class="form-control" id="service_status"
                            value='<?php echo $service_status ?>' placeholder="Select Service Status">
                        <option>Select</option>
                        <option>Available</option>
                        <option>Unavailable</option>
                        <option>Limited Capacity</option>
                    </select>
                </div>
                <div class="form-group">
                    <!-- Price input-->
                    <label for="serviceQuantity">Service Quantity</label>
                    <input type="text" class="form-control" id="service_quantity"
                           value='<?php echo $service_quantity ?>' placeholder="Enter Service Quantity"
                           name="service_quantity">
                </div>
                <div class="form-group">
                    <!-- Id -->
                    <input type="hidden" class="form-control" id="serviceId" value='<?php echo $serviceId ?>'
                           name="serviceId">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Update Service</button>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<?php
}
?>
</body>
</html>
