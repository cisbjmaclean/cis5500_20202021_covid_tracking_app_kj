<?php
session_start();

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Admin Home Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


require_once ("../Data Access Object/connectDAO.php");
require_once ("../Other/footer.php");
require_once ("../Other/header.php");


global $mysqli;
if (isset($_GET['userAccessId'])) {
    $adminId = $_GET['userAccessId'];
    $_SESSION['SESS_ID'] = $adminId;
    $_SESSION['activeLogin'] = true;
}


//Gets current date and time
function getCurrentTime() {

    date_default_timezone_set('Canada/Atlantic');
    $month = date('M-d-Y');
    $time = date('h:i A');
    echo $month . " at " . $time;

}
/**if (isset($_POST['submit'])) {
    if (count($_POST) > 0) {


        $getUserLogin = $mysqli->prepare("SELECT userAccessId, fullName FROM useraccess WHERE userAccessId = '$adminId'");
        $getUserLogin->execute();
        $results = $getUserLogin->get_result();

//Display employee details from the database
        if ($results->num_rows > 0) {
            // output data of each row
            while ($row = $results->fetch_assoc()) {

                $adminUserId = $row["userAccessId"];
                $adminName = $row["fullName"];


            }


            //Display a message if there is no records or results
        } else {
            echo "<h1>There is no records to display at this time</h1>";
//Free the memory from the server
            $getUserLogin->free_result();

//Close the database
            $mysqli->close();

            exit();
        }
    }
}**/
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>K & J | Administrator Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<?php
echo adminNavbar();
?>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <h4>Guests:</h4>

                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="adminService.php">
                            <img src="../COVID-19%20Tracking%20Media/img/clipboard-with-pencil-.png">
                            Register User
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="checkEvent.php">
                            <img src="../COVID-19%20Tracking%20Media/img/approved-signal.png">
                            Validate User
                        </a>
                    </li>
                    <h4>Services:</h4>
                    <li class="nav-item">
                        <a class="nav-link" href="createService.php">
                            <img src="../COVID-19%20Tracking%20Media/img/edit.png">
                            Create Service
                        </a>
                    </li>
                    <h4>User:</h4>
                    <li class="nav-item">
                        <a class="nav-link" href="createUser.php">
                            <img src="../COVID-19%20Tracking%20Media/img/user-image-with-black-background.png">
                            Create New User
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="viewUsers.php">
                            <img src="../COVID-19%20Tracking%20Media/img/view-details.png">
                            View User Account
                        </a>
                    </li>

                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-center flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h2>Administrator</h2>
                    <?php
                    echo " Logged in: ";
                    echo getCurrentTime();
                    ?>




            </div>

            <h2 id="service-heading">Current Services</h2>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Service ID</th>
                        <th>Service Name</th>
                        <th>Service Description</th>
                        <th>Service Date</th>
                        <th>Service Time</th>
                        <th>Service Status</th>
                        <th>Service Quantity</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <?php

                    global $mysqli;
                    $viewCurrentServices = $mysqli->prepare("SELECT * FROM service ORDER BY serviceId");
                    $viewCurrentServices->execute();
                    $results = $viewCurrentServices->get_result();

                    //Display employee details from the database
                    if ($results->num_rows > 0) {
                        // output data of each row
                        while ($row = $results->fetch_assoc()) {

                            $serviceID = $row["serviceId"];
                            $serviceName = $row["service_name"];
                            $serviceDesc = $row["service_description"];
                            $serviceDate = $row["service_date"];
                            $serviceTime = $row["service_time"];
                            $serviceStatus = $row["service_status"];
                            $serviceQty = $row["service_quantity"];


                        if ($serviceQty > 25) {

                            echo
                                "<tr><td class='table-success'>$serviceID</td>
                    <td class='table-success'>$serviceName</td>
                    <td class='table-success'>$serviceDesc</td>
                    <td class='table-success'>$serviceDate</td>
                    <td class='table-success'>$serviceTime</td>
                    <td class='table-success'>$serviceStatus</td>
                    <td class='table-success'>$serviceQty</td>
                    <td class='table-success'><a href='editService.php?serviceId=" . $serviceID . "' title='Edit Service'><img src='../COVID-19%20Tracking%20Media/img/edit.png' alt='Edit'></a></td><td class='table-success'> <a href='deleteService.php?serviceId=" . $serviceID . "' title='Delete Service' ><img src='../COVID-19%20Tracking%20Media/img/trash-bin.png' alt='Delete'></a></td>
               
              
                   
               
               </tr>";
                        }



               if ($serviceQty <= 25 && $serviceQty > 10) {
                            echo
                                "<tr><td class='table-warning'>$serviceID</td>
                    <td class='table-warning'>$serviceName</td>
                    <td class='table-warning'>$serviceDesc</td>
                    <td class='table-warning'>$serviceDate</td>
                    <td class='table-warning'>$serviceTime</td>
                    <td class='table-warning'>$serviceStatus</td>
                    <td class='table-warning'>$serviceQty</td>
                    <td class='table-warning'><a href='editService.php?serviceId=" . $serviceID . "' title='Edit Service'><img src='../COVID-19%20Tracking%20Media/img/edit.png' alt='Edit'></a></td><td class='table-warning'> <a href='deleteService.php?serviceId=" . $serviceID . "' title='Delete Service' ><img src='../COVID-19%20Tracking%20Media/img/trash-bin.png' alt='Delete'></a></td>
               
              
                   
               
               </tr>";
                        }

                        if ($serviceQty < 10 || $serviceQty = 0) {
                            echo
                                "<tr><td class='table-danger'>$serviceID</td>
                    <td class='table-danger'>$serviceName</td>
                    <td class='table-danger'>$serviceDesc</td>
                    <td class='table-danger'>$serviceDate</td>
                    <td class='table-danger'>$serviceTime</td>
                    <td class='table-danger'>$serviceStatus</td>
                    <td class='table-danger'>$serviceQty</td>
                    <td class='table-danger'><a href='editService.php?serviceId=" . $serviceID . "' title='Edit Service'><img src='../COVID-19%20Tracking%20Media/img/edit.png' alt='Edit'></a></td><td class='table-danger'> <a href='deleteService.php?serviceId=" . $serviceID . "' title='Delete Service' ><img src='../COVID-19%20Tracking%20Media/img/trash-bin.png' alt='Delete'></a></td>
               
              
                   
               
               </tr>";
                        }


                        }

                        //Display a message if there is no records or results
                    } else {
                        echo "<h1>There is no records to display at this time</h1>";
                        exit();
                    }
                    echo "</table>
            </div>";
                    //Free the memory from the server
                    $viewCurrentServices->free_result();

                    //Close the database
                    $mysqli->close();

                    ?>
            </div>
        </main>
    </div>
    <?php
    echo displayFooter();
    ?>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script></body>
</html>

