<?php
// Author: Elikem K. Akatti Junior
// Purpose: Delete book page
// Date: November 27, 2020

session_start();
// set up connection
require_once ("../Data Access Object/connectDAO.php");
require_once ("../Other/header.php");

global $mysqli;
$bookId = "";
$msg = "";
// Process delete operation after confirmation
if (isset($_GET["serviceId"]) && !empty($_GET["serviceId"])) {
    //Sanitize the parameter
$userId = $_SESSION['SESS_ID'];

    $serviceId = $mysqli->real_escape_string($_GET['serviceId']);
    // example UPDATE query
    $query = "DELETE FROM service WHERE service.serviceId =$serviceId ";
    $result = $mysqli->query($query);

    if ($result) {
        $msg = "Service deleted successfully. ".$mysqli->affected_rows . " service deleted from database. <a href='adminUser.php'>View all Services</a>";

    } else {
        $msg = "Error deleting service: " . $mysqli->error;
    }

    $mysqli->close();

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <?php
    // Process delete operation after confirmation
    if (isset($_GET["serviceId"])) {
        //Sanitize the parameter
        $userId = $_SESSION['SESS_ID'];

        echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?userAccessId=$userId'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
    }
    ?>
    <h2>Service Deleted</h2>
    <p class="error"><?php echo $msg ?></p>
</div>
</body>
</html>
