<?php

// Author: Elikem K. Akatti Junior
// Purpose: Update page that update s data
// Date: November 27, 2020


session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <title>K & J - Update</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/custom.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div id="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">

            <div class="navbar-header navbar-right">
                <a class="navbar-brand" href="../logout.php">Logout</a>
            </div>
        </div>
    </nav>
    <h1>Book-O-Rama</h1>
    <?php

    ?>
</div>
</body>
</html>