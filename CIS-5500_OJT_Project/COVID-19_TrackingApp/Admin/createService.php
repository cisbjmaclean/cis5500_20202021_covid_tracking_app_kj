<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Create Service Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */

session_start();


require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");
require_once ("../Other/header.php");

checkServiceForm();


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Administrator | Create Service</title>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
          rel="stylesheet" type="text/css"/>
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->

    <link href="../css/customStyles.css" rel="stylesheet"/>

</head>
<body id="page-top">
<!-- Navigation-->
    <?php
    echo displayNavbar();
    ?>
<main role="main">
    <div id="container">

        <div class="album py-5 bg-light">
            <div class="form-row">
                <h2 id="create-service-heading">KJM | Create Service</h2>
                <form action="" method="post">
                    <div id="service-inner-div">
                        <div class="col-md-6 mb-3">
                            <label for="service-name">Service Name:</label>
                            <input type="text" class="form-control" name="service-name" id="guest-fname"
                                   placeholder="Name">

                        </div>

                        <div class="col-md-6 mb-3">
                            <label for="service-date">Service Date</label>
                            <input type="text" class="form-control" name="service-date" id="guest-room"
                                   placeholder="YYYY-MM-DD">


                            <div class="col-md-6 mb-3">
                                <label for="service-time">Service Time</label>
                                <input type="text" class="form-control" name="service-time" id="city"
                                       placeholder=" e.g. 09:00">
                            </div>


                            <div class="col-md-6 mb-3">
                                <label for="service-description">Service Description</label>
                                <input type="text" class="form-control" name="service-description" id="depart-date"
                                       placeholder="Details">

                                <label for="service-status">Service Status:</label>
                                <select name="service-status" id="service-stat" class="custom-select">
                                    <option>Select</option>
                                    <option>Available</option>
                                    <option>Unavailable</option>
                                    <option>Limited Capacity</option>
                                </select>

                                <label for="service-qty">Service Capacity:</label>
                                <select name="service-qty" id="service-stat" class="custom-select">
                                    <option>Select</option>
                                    <?php
                                    for ($counter = 0; $counter <= 50; $counter++) {
                                        echo "<option>$counter</option>";
                                    }
                                    ?>
                                </select>


                            </div>
                            <button type="submit" id="btn-create-service" name="btn-submit"
                                    class="btn btn-success my-2">Complete Service
                            </button>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</body>
</html>


<!-- Bootstrap core JS-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>
</html>



