<?php

/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Edit User Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 */


session_start();

// connect to db
require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");
require_once("../Other/header.php");

updateUserInfo();

// extract the GET variable isbn
if (isset($_GET['userAccessId'])) {

    //they have an isbn in the url
    $userId = $_GET['userAccessId'];


    global $mysqli;

    $userId = $mysqli->real_escape_string($userId);

    // get the data for just the book we want to edit!
    $query = "SELECT useraccess.userAccessId, username, pass_word, fullName, 
useraccessdetail.phoneNumber, useraccessdetail.userAccessId FROM useraccess INNER JOIN useraccessdetail
ON useraccess.userAccessId = useraccessdetail.userAccessId 
WHERE useraccess.userAccessId='$userId'";
    $result = $mysqli->query($query);

    if ($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            $user_name = $row['username'];
            $user_password = $row['pass_word'];
            $user_fullName = $row['fullName'];
            $phoneNumber = $row["phoneNumber"];

        }

        $result->free();
        $mysqli->close();
    } else {
        //the id is not provided
        $message = "Sorry, no id provided.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>K & J| Edit User</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<?php
echo displayNavbar();
?>
<br>
<br>
<br>
<div id="container">
    <div class="album py-5 bg-light">
        <div class="form-row">
            <form action="" method="post">

                <div class="col-md-4">
                    <h2 class="customer-heading"> User Information</h2>
                    <div class="col-md-4">
                        <input type="text" id="fullName" class="form-control" value='<?php echo $user_fullName ?>'
                               name="user-fullname">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="tel" id="phoneNumber" class="form-control" value='<?php echo $phoneNumber ?>'
                               name="user-number">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="text" id="user-Name" class="form-control" value='<?php echo $user_name ?>'
                               name="user-username">
                    </div>
                    <br>
                    <div class="col-md-4">
                        <input type="password" id="password" class="form-control" value='<?php echo $user_password ?>'
                               name="user-password">
                    </div>
                    <br>


                </div>


        </div>
        <button type="submit" id="btn-guest-booking" class="btn btn-outline-primary" name="btn-user">Submit</button>


        </form>
    </div>

</div>
</div>
</body>
</html>

