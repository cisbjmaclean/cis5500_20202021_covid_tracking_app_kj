<?php
/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - Home Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 *
 *
 * This file is the main home page for the Covid Tracking App.
 * Here, the user can navigate to any part of the system
 */

//Starts the session
session_start();


//Allows access to use functions in other files
require_once("Admin/validateForms.php");
require_once("Data Access Object/connectDAO.php");
require_once("Other/footer.php");

//Database variable
global $mysqli;

//Select the brand name from the database to display on the home page
$viewBrandName = $mysqli->prepare("SELECT codeTypeId, codeValueSequence, englishDescription FROM codevalue
WHERE codeTypeId = 3 AND codeValueSequence = 1");
$viewBrandName->execute();
$results = $viewBrandName->get_result();

if ($results->num_rows > 0) {
    // output data of each row
    while ($row = $results->fetch_assoc()) {

        //Capture brand name for database row
        $brandName = $row["englishDescription"];


    }


    //Display a message if there is no records or results
} else {
    echo "<h1>There is no records to display at this time</h1>";
    exit();
}

//Free the memory from the server
$viewBrandName->free_result();

//Select the welcome message from the database to display on the home page
$viewWelcomeMessage = $mysqli->prepare("SELECT codeTypeId, codeValueSequence, englishDescription FROM codevalue
WHERE codeTypeId = 3 AND codeValueSequence = 2");
$viewWelcomeMessage->execute();
$results = $viewWelcomeMessage->get_result();


if ($results->num_rows > 0) {
    // output data of each row
    while ($row = $results->fetch_assoc()) {

        //Capture welcome message for database row
        $welcomeMessage = $row["englishDescription"];


    }


    //Display a message if there is no records or results
} else {
    echo "<h1>There is no records to display at this time</h1>";
    exit();
}

//Free the memory from the server
$viewWelcomeMessage->free_result();
$mysqli->close();


/**
 * This function check if  the user logged in and displays the correct navbar based on user roles
 * If the user isn't logged in as yet, the default navbar will displayed
 */
function checkUserSessionStatus()
{

    //Checks if the session variable is set after logging in
    if (isset($_SESSION['activeLogin'])) {
        //Sets the session variable used to capture user access based on
        $id = $_SESSION['SESS_ID'];
        global $brandName;

//Checks the user access id to display the specific navbar (Admin or User)
        if (($id == 1) || ($id == 2)) {
            echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
       <a class='navbar-brand js-scroll-trigger' href='#page-top'>$brandName</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
            <li class='nav-item'><a class='nav-link' href='./User/user.php?userAccessId=$id'><img 
            src='./COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='./User%20Login/userLogin.php?id=$id'><img src='./COVID-19%20Tracking%20Media/img/exit.png' alt='Logout' name='user-logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
        } else if (($id == 3) || ($id == 4)) {
            echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
       <a class='navbar-brand js-scroll-trigger' href='#page-top'>$brandName</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
            <li class='nav-item'><a class='nav-link' href='./Admin/adminUser.php?userAccessId=$id'><img
                                src='./COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='./User%20Login/userLogin.php?id=$id'><img src='./COVID-19%20Tracking%20Media/img/exit.png' alt='Logout' name='user-logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
        } //Checks if the session is active and the user access id is set
        else if ((session_status() === PHP_SESSION_ACTIVE) && (!isset($id))) {
            echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='#page-top'>$brandName</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse'
                data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false'
                aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./UI/services.php'>Services</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./UI/createGuest.php'>Sign Up</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./User%20Login/userLogin.php'>Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>";
        }

    } //Checks if the session is not active and displays the default navbar
    else {
        global $brandName;
        echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='#page-top'>$brandName</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse'
                data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false'
                aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./UI/services.php'>Services</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./UI/createGuest.php'>Sign Up</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='./User%20Login/userLogin.php'>Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>";
    }

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>KJ Ministries</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
          rel="stylesheet" type="text/css"/>
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="css/customStyles.css" rel="stylesheet"/>

</head>
<body id="page-top">
<?php

//Displays the navbar based on session status
echo checkUserSessionStatus();
?>
<!---Masthead--->
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-lg-10 align-self-end">
                <h1 class="text-uppercase text-white font-weight-bold"><?php
                    echo "Welcome to " . $brandName;
                    ?></h1>
                <hr class="divider my-4"/>
            </div>
            <div class="col-lg-8 align-self-baseline">
                <p class="text-white-75 font-weight-light mb-5"><?php
                    echo $welcomeMessage;
                    ?></p>

            </div>
        </div>
    </div>
</header>
<!-- About-->
<section class="page-section" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">
                <h2 class="text-white mt-0">About Us</h2>
                <hr class="divider  my-4"/>
                <p class="text-white mb-4">Located in Charlottetown, PEI, our mission is to bring locals closer to God
                    by spreading the word.</p>
                <p class="text-white mb-4">We offer a variety of experiences through our numerous Services.</p>
                <p class="text-white mb-4">Together, we all can save lives by sharing the gospel!</p>


            </div>
        </div>
    </div>
</section>
<!-- Services-->
<section class="page-section " id="services">
    <div class="container">
        <h2 class="text-center mt-0">Services we offer</h2>
        <hr class="divider light my-4"/>
        <div class="row" id="service-section">
            <div class="col-lg-3 col-md-6 text-center" id="service-container1">
                <div class="mt-5">
                    <img src="./COVID-19%20Tracking%20Media/img/speaker%20(2).png">
                    <h3 class="h4 mb-2">Great Services</h3>
                    <p class="text-white mb-0">Well-respected pastors and spokesmen that deliver professional and
                        intellectual services.</p>
                </div>


            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <img src="./COVID-19%20Tracking%20Media/img/pray.png">
                    <h3 class="h4 mb-2">Praise & Worship</h3>
                    <p class="text-white mb-0">Sing along with our wonderful choir to some of your favorite hymns</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <img src="./COVID-19%20Tracking%20Media/img/bible.png">
                    <h3 class="h4 mb-2">Bible Study</h3>
                    <p class="text-white mb-0">Sharpen your bible skills with our well-educated teachers</p>
                </div>

            </div>
            <div class="col-lg-3 col-md-6 text-center">
                <div class="mt-5">
                    <img src="./COVID-19%20Tracking%20Media/img/praying.png">
                    <h3 class="h4 mb-2">Prayer Meeting</h3>
                    <p class="text-white mb-0">Put in some personal time with God by spending a few minutes in
                        prayer</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact-->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 text-center">

                <h2 class="mt-0">Spread the Word!</h2>
                <hr class="divider bg-secondary my-4"/>
                <p class="text-muted mb-5">Do you enjoy your time at K & J Ministries? Let others know about the gospel
                    by sharing your experience.
                    Contact us below for further information or advice!</p>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                <i class="fas fa-phone fa-3x mb-3 text-muted"></i>
                <div>Kevin: +1 (902) 598-9546</div>
                <div>Junior: +1 (902) 916-4929</div>
            </div>
            <div class="col-lg-4 mr-auto text-center">
                <i class="fas fa-envelope fa-3x mb-3 text-muted"></i>
                <!-- Make sure to change the email address in BOTH the anchor text and the link target below!-->
                <p class="d-block">ktaylor123070@hollandcollege.com</p>
                <p class="d-block">eakatti@hollandcollege.com</p>
            </div>
        </div>
    </div>
</section>
<!-- Footer-->
<?php
//Displays the footer
echo displayFooter();
?>
<!-- Bootstrap core JS-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>
</html>

