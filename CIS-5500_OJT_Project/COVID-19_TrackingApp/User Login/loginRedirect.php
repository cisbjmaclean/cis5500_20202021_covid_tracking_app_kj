<?php


require_once ("../Data Access Object/connectDAO.php");



//Gets current date and time
function getCurrentTime() {

    date_default_timezone_set('Canada/Atlantic');
    $month = date('M-d-Y');
    $time = date('h:i A');
    echo $month . " at " . $time;

}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>K & J | User Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.html">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../index.html"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <h4>Guests:</h4>
                        <a class="nav-link active" href="#">
                            <img src="../COVID-19%20Tracking%20Media/img/user-image-with-black-background.png">

                            Create New Guest
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../UI/viewGuest.php">
                            <img src="../COVID-19%20Tracking%20Media/img/view-details.png">
                            View Guest
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="../COVID-19%20Tracking%20Media/img/approved-signal.png">

                            Validate Guest
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <img src="../COVID-19%20Tracking%20Media/img/draw.png">
                            Edit Guest Account
                        </a>
                    </li>
                    </li>
                </ul>
            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">User</h1>

                <?php
                echo "You logged in ";
                echo getCurrentTime();
                ?>

                <div class="btn-toolbar mb-2 mb-md-0">

                </div>
            </div>

            <h2>Current Services</h2>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Service ID</th>
                        <th>Service Name</th>
                        <th>Service Date</th>

                        <th>Service Time</th>
                        <th>Service Status</th>

                    </tr>
                    </thead>
                    <?php

                    $userName = $mysqli->real_escape_string($_POST['userName']);
                    $password = $mysqli->real_escape_string($_POST['password']);


                    $query = "SELECT username FROM useraccess WHERE username=" . $userName . " AND password('" . $password . "')";

                    $result = $mysqli->query($query);

                    if ($result = $mysqli->query($query)) {

                        if($result->num_rows ==1) {

                            if(($userName == "akattijnr"  && $password == "akattijnr") || ($userName == "ktaylor" && $password == "ktaylor")){
                                header("location:  ../Admin/adminUser.php");
                            }else{
                                header("location: ../User/user");
                            }

                        }

                    }

                    ?>
            </div>
        </main>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script></body>
</html>
