<?php
/**
 *Author: Kevin Taylor
 * Date: 04/23/2021
 * Subject: CIS-5500 OJT Project - User Login Page
 *Instructor: Donnie McKinnon, Joey Kitson, BJ MacLean
 *
 *
 * This file stores the user input controls to login into the system using username
 * and password form the database
 */

//Starts the session
session_start();


//Allows access to use functions and variables in other files
require_once("../Admin/validateForms.php");
require_once("../Other/footer.php");
require_once("../Data Access Object/connectDAO.php");


//Checks if the user clicked the submit button and validates the fields
if (isset($_POST['submit'])) {
    if (count($_POST) > 0) {

        //Database variable
        global $mysqli;
        $message = "";

        //Selects all the usernames and passwords from the database to compare user input to the database records
        $getLoginInfo = $mysqli->prepare("SELECT * FROM useraccess 
WHERE username='" . $_POST["userName"] . "' and pass_word = '" . $_POST["password"] . "'");
        $getLoginInfo->execute();
        $results = $getLoginInfo->get_result();

//Display user access details from the database
        if ($results->num_rows > 0) {
// output data of each row
            while ($row = $results->fetch_assoc()) {


                //Captures each row
                $user_Name = $row["username"];
                $pass_Word = $row["pass_word"];
                $user_Id = $row["userAccessId"];
                $user_StatusCode = $row["userAccessStatusCode"];
                $user_TypeId = $row["userTypeCode"];


                //Captures user input from the form
                $userName = $_POST["userName"];
                $password = $_POST["password"];

                $message = "You are successfully authenticated!";


                //Checks if user entered in the correct username and password and redirects them to their respective user page (Admin or User)
                if (($userName == $user_Name) && ($password == $pass_Word) && ($user_TypeId == 2)) {
                    header("location: ../Admin/adminUser.php?userAccessId=$user_Id");
                } else if (($userName == $user_Name) && ($password == $pass_Word) && ($user_TypeId == 1)) {
                    header("location: ../User/user.php?userAccessId=$user_Id");
                    //If nothing matches, redirect the user to the log in page
                } else {
                    header("location: ../User Login/userLogin.php");
                    $message = "Invalid Username or Password!";
                    echo "<div class='message'>$message</div>";
                }


            }

        }
    }
}

//Destroys the session if the user clicked on any logout button
if (isset($_GET['id'])) {
    session_destroy();
}
if (isset($_GET['SESS_ID'])) {
    session_destroy();
}
if (isset($_GET['user-logout'])) {
    session_destroy();
}
if (isset($_GET['userAccessId'])) {
    session_destroy();
}

?>
<html>
<head>
    <title>User Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>KJ Ministries Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/customStyles.css" rel="stylesheet">

</head>

<body class="text-center">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../welcome.php">Home </a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../UI/services.php">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../UI/createGuest.php">Sign Up</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>
<form name="form-signin" method="post" action="">
    <img src="../COVID-19%20Tracking%20Media/img/k&j-logo.PNG" alt="K&J Ministries">
    <h1 class="h3 mb-3 font-weight-normal">User Login</h1>


    <td align="center" colspan="2">Enter Login Details</td>

    <input type="text" id="user-login-name" name="userName" placeholder="User Name" class="form-control"></td>
    <br>
    <input type="password" id="user-login-password" name="password" placeholder="Password" class="form-control"></td>
    <br>
    <input type="submit" name="submit" value="Submit" class="btnSubmit">


    <?php
    //Display footer
    echo displayFooter();
    ?>
</form>
</body>
</html>