<?php
session_start();
//Starts session to ensure user is logged in


require_once ("../Data Access Object/connectDAO.php");
require_once ("../Admin/validateForms.php");
require_once ("../Other/header.php");

if(isset($_SESSION['SESS_ID'])) {
    $SESS_ID = $_SESSION['SESS_ID'];
} else {
    echo "Sessions variables are not set";
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="veggieStyles.css">
    <title>Registration Confirm</title>
</head>

<body>

<?php
if (($SESS_ID == 3) || ($SESS_ID == 4)) {

    echo"<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
             <li class='nav-item'><a class='nav-link' href='../Admin/adminUser.php?userAccessId=$SESS_ID'><img
                                src='../COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?SESS_ID=$SESS_ID' name='user-logout'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";

} else if (($SESS_ID == 1) || ($SESS_ID == 2)) {
    echo"<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
             <li class='nav-item'><a class='nav-link' href='../User/user.php?userAccessId=$SESS_ID'><img
                                src='../COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?SESS_ID=$SESS_ID' name='user-logout'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
}
?>
<div class="container" id="bookingResults">
    <div class="py-5 text-center">
        <h2>Service Registration Complete <img src="../COVID-19%20Tracking%20Media/img/check.png"></h2>
        <p class="lead">You're now reserved for the selected event!
            Please ensure to wear a mask to the event and maintain social distancing at all times.</p>
        <h2>Registration Overview</h2>
    </div>

   <!----- <div class="row">
        <div class="row2">
            <div class="inner-div2">
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div class="invoice">
                            <h6 class="my-0">Congregant Name: </h6>

                        </div>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div class="invoice">
                            <h6 class="my-0">Event: </h6>

                        </div>
                    </li>
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="invoice">
                            <h6 class="my-0">Phone Number: </h6>

                        </div>
                    </li>

                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="invoice">
                            <h6 class="my-0">Time of Registration: </h6>

                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </div>---->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="form-validation.js"></script>
</div>
</body>

</html>
