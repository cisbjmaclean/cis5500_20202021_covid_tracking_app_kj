<?php
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.html">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../index.html">Home </a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../UI/services.php">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../User%20Login/userLogin.php">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>
<div id="container">
    <div class="form-row">
        <form action="completeBooking.php" method="post" class="this">
            <div class="container2">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="customer-heading"> Guest Information</h2>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="First Name" name="cust-fname">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Last Name" name="cust-lname">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="email" class="form-control" placeholder="Email" name="cust-email">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="tel" class="form-control" placeholder="Phone Number (902-555-5555)"
                                   name="cust-number">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">

                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-outline-primary" name="btn-orders">Complete Booking</button>
            </div>


        </form>
    </div>

</body>
</html>

