<?php

//Starts session to ensure user is logged in
session_start();

require_once ("../Other/header.php");


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="veggieStyles.css">
    <title>Booking Confirm</title>
</head>

<body>
<!-- Navigation-->

<?php
echo displayNavbar();
?>
<div class="container" id="bookingResults">
    <div class="py-5 text-center">
        <h2>Validation Complete <img src="../COVID-19%20Tracking%20Media/img/check.png"></h2>
        <p class="lead">All guest are now accounted for!
            Return to the Home Page or valid another service</p>
       <!----<h2>Registration Overview</h2>---->
    </div>

    <!----- <div class="row">
         <div class="row2">
             <div class="inner-div2">
                 <ul class="list-group mb-3">
                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                         <div class="invoice">
                             <h6 class="my-0">Congregant Name: </h6>

                         </div>
                     </li>
                     <li class="list-group-item d-flex justify-content-between lh-condensed">
                         <div class="invoice">
                             <h6 class="my-0">Event: </h6>

                         </div>
                     </li>
                     <li class="list-group-item d-flex justify-content-between bg-light">
                         <div class="invoice">
                             <h6 class="my-0">Phone Number: </h6>

                         </div>
                     </li>

                     <li class="list-group-item d-flex justify-content-between bg-light">
                         <div class="invoice">
                             <h6 class="my-0">Time of Registration: </h6>

                         </div>
                     </li>
                 </ul>

             </div>
         </div>
     </div>---->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="form-validation.js"></script>
</div>
</body>

</html>

