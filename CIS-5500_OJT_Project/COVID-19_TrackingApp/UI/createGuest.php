<?php

//Starts session to ensure user is logged in
session_start();

require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");

checkGuestForm();



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>K & J Ministries | Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../welcome.php">Home </a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../UI/services.php">Services</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger"
                                        href="../User%20Login/userLogin.php">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>
<div id="container">
    <div class="album py-5 bg-light">
    <div class="form-row">
        <form action="" method="post">

                <div class="row">
                    <div class="col-md-4">
                        <h2 class="customer-heading"> Guest Information</h2>
                        <div class="col-md-4">
                            <input type="text" id="new-guest-name" class="form-control" placeholder="Full Name" name="guest_fullname">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="tel" id="new-guest-number" class="form-control" placeholder="Phone Number (902-555-5555)"
                                   name="guest_number">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <label id="user-types-label" for="user-type">User Types:</label>
                            <select name="code-values" id="user-type" class="custom-select">
                                <option>Select</option>
                                <?php
                                global $mysqli;
                                $getUserTypes = $mysqli->prepare("SELECT codeTypeId, codeValueSequence, englishDescription FROM codevalue
WHERE codeTypeId = 1 AND codeValueSequence = 1 OR codeTypeId = 1 AND codeValueSequence = 2 ");
                                $getUserTypes->execute();
                                $results = $getUserTypes->get_result();

                                //Display employee details from the database
                                if ($results->num_rows > 0) {
                                    // output data of each row
                                    while ($row = $results->fetch_assoc()) {

                                        $userTypes = $row["englishDescription"];

                                        echo "
                          
<option>$userTypes</option>

";


                                    }


                                    //Display a message if there is no records or results
                                } else {
                                    echo "<h1>There is no records to display at this time</h1>";
                                    exit();
                                }

                                //Free the memory from the server
                                $getUserTypes->free_result();
                                $mysqli->close();
                                ?>
                            </select>
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="text" id="new-guest-username" class="form-control" placeholder="Username" name="guest_username">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="password" id="new-guest-password" class="form-control" placeholder="New Password"
                                   name="guest_password">
                        </div>
                        <br>
                        <div class="col-md-4">
                            <input type="password" id="new-guest-con-password" class="form-control" placeholder="Confirm Password"
                                   name="guest_new_password">
                        </div>
                        <br>

                    </div>


    </div>
    <button type="submit" id="btn-new-guest" class="btn btn-outline-primary" name="btn-booking">Submit</button>
</div>


</form>

    </div>
</div>
</body>
</html>

