<?php

//Starts session to ensure user is logged in
session_start();


require_once ("../Data Access Object/connectDAO.php");
require_once("../Other/header.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="veggieStyles.css">
    <title>Booking Confirm</title>
</head>

</head>
<body>


<?php

if (!isset($_SESSION['activeLogin'])){

    echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../welcome.php'>Home </a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='../UI/services.php'>Services</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='../User%20Login/userLogin.php'>Login</a></li>
            </ul>
        </div>
    </div>
    </nav>
    <br>
<br>
<br>
";


}else if (isset($_SESSION['activeLogin'])) {
echo displayNavbar();
}
?>

<div class="container" id="bookingResults">
    <div class="py-5 text-center">
        <h2>Guest Registration Complete <img src="../COVID-19%20Tracking%20Media/img/check.png"></h2>
        <p class="lead">You now have access to our system!
            Use your credentials entered to login to the system.</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <script src="form-validation.js"></script>
</div>
</body>

</html>

