<?php



require_once ("../Data Access Object/connectDAO.php");




?>



<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
    <title>View Services</title>

</head>
<body>


<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../welcome.php">Home </a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../UI/createGuest.php">Sign Up</a></li>
                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="../User%20Login/userLogin.php">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<br>
<br>
<br>
<div class="container">

    <main role="main">
        <div class=" pt-3 pb-2 mb-3">
            <h3 id="services-heading">Available Services</h3>


        </div>


    </main>




    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Service ID</th>
            <th>Service Name</th>
            <th>Service Description</th>
            <th>Service Date</th>
            <th>Service Time</th>
            <th>Service Status</th>
            <th>Service Quantity</th>
        </tr>
        </thead>


        <?php

        global $mysqli;
        $viewCurrentServices = $mysqli->prepare("SELECT serviceId, service_description, service_name, service_date, service_time, service_status, service_quantity FROM service ORDER BY serviceId");
        $viewCurrentServices->execute();
        $results = $viewCurrentServices->get_result();

        //Display employee details from the database
        if ($results->num_rows > 0) {
            // output data of each row
            while ($row = $results->fetch_assoc()) {

                $serviceId = $row["serviceId"];
                $serviceName = $row["service_name"];
                $serviceDesc = $row["service_description"];
                $serviceDate = $row["service_date"];
                $serviceTime = $row["service_time"];
                $serviceStatus = $row["service_status"];
                $serviceQty = $row["service_quantity"];


                    echo
                    "<tr><td>$serviceId</td>
                        <td>$serviceName</td>
                        <td>$serviceDesc</td>
                        <td>$serviceDate</td>
                        <td>$serviceTime</td>
                        <td>$serviceStatus</td>
                        <td>$serviceQty</td>
                      </tr>
                    ";

                }



            //Display a message if there is no records or results
        } else {
            echo "<h1>There is no records to display at this time</h1>";
            exit();
        }
        echo "</table>
            </div>";
        //Free the memory from the server
        $viewCurrentServices->free_result();

        //Close the database
        $mysqli->close();

        ?>
</div>
</body>
</html>
