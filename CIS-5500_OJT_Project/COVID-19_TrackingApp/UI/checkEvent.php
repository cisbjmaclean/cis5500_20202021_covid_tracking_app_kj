<?php

/**
 * Author: Kevin Taylor
 * Date: 03/12/2021
 * Subject: CIS-2261 Final Project Application - Guest Service Validation Page
 * Instructor: Joey Kitson
 *
 *
 *This file contains the Guest Validation page to validate a guest
 * based on their age for a service
 *
 *
 */

//Starts session to ensure User is logged in
session_status();


require_once ("../Data Access Object/connectDAO.php");


//Destroys the session when the User clicks the logout button
if (isset($_POST['btn-logout'])) {
    session_destroy();
}
?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>K & J | Administrator Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../index.html">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../Admin/adminUser.php"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">

    <div class="py-5 text-center">
        <h4 class="mb-3">Event Validation</h4>
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Service ID</th>
                <th>Service Name</th>
                <th>Service Date</th>
                <th>Service Time</th>
                <th>Action</th>
            </tr>
            </thead>


            <?php

            global $mysqli;
            $viewCurrentServices = $mysqli->prepare("SELECT serviceId, service_name, service_date, service_time FROM service ORDER BY serviceId");
            $viewCurrentServices->execute();
            $results = $viewCurrentServices->get_result();

            //Display employee details from the database
            if ($results->num_rows > 0) {
                // output data of each row
                while ($row = $results->fetch_assoc()) {

                    $serviceId = $row["serviceId"];
                    $serviceName = $row["service_name"];
                    $serviceDate = $row["service_date"];
                    $serviceTime = $row["service_time"];




                        echo
                        "<tr><td>$serviceId</td>
                        <td>$serviceName</td>
                        <td>$serviceDate</td>
                        <td>$serviceTime</td>
                     
                        <td><a href='validateGuest.php?serviceId=$serviceId'>View</a></td>
                    </tr>
                    ";



                }

                //Display a message if there is no records or results
            } else {
                echo "<h1>There is no records to display at this time</h1>";
                exit();
            }
            //Free the memory from the server
            $viewCurrentServices->free_result();

            //Close the database
            $mysqli->close();

            ?>

        </table>


    </div>

    <footer class="bg-light py-5">
        <div class="container">
            <div class="small text-center text-muted">SleepEasy Hotel &copy; 2021 </div>
        </div>
    </footer>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
<script src="form-validation.js"></script></body>
</html>