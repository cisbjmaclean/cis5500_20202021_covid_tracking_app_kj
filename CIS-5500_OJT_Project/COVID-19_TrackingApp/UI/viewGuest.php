<?php

//Starts session to ensure user is logged in
session_status();


require_once("../Data Access Object/connectDAO.php");





?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>KJ Ministries</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico"/>
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
          rel="stylesheet" type="text/css"/>
    <!-- Third party plugin CSS-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
          rel="stylesheet"/>
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet"/>
    <link href="../css/customStyles.css" rel="stylesheet"/>

</head>
<body id="page-top">
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="../Admin/adminUser.php"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>

<!-----Change in CSS------>
<br>
<br>
<br>
<table class="table">
    <thead class="thead-dark" id="guest-table-header">
    <tr>
        <th scope="col">User ID</th>
        <th scope="col">Username</th>
        <th scope="col">Password</th>
        <th scope="col">Full Name</th>
        <th scope="col">Phone Number</th>

        <th scope="col">Action</th>

    </tr>
    </thead>
    <?php

    global $mysqli;
    $viewCurrentGuests = $mysqli->prepare("SELECT useraccess.userAccessId, username, password, fullName, 
useraccessdetail.phoneNumber, useraccessdetail.userAccessId FROM useraccess INNER JOIN useraccessdetail
ON useraccess.userAccessId = useraccessdetail.userAccessId");
    $viewCurrentGuests->execute();
    $results = $viewCurrentGuests->get_result();

    //Display employee details from the database
    if ($results->num_rows > 0) {
        // output data of each row
        while ($row = $results->fetch_assoc()) {

            $userID = $row["userAccessId"];
            $userName = $row["username"];
            $userPassword = $row["password"];
            $guestName = $row["fullName"];
            $phoneNumber = $row["phoneNumber"];

            $encryptPassword = password_hash($userPassword, PASSWORD_DEFAULT);



            echo
            "<tr><td>$userID</td>
                    <td>$userName</td>
                    <td>$userPassword</td>
                    <td>$guestName</td>
                    <td>$phoneNumber</td>
                    
                    <td><a href='userEdit.php'><img src='../COVID-19%20Tracking%20Media/img/edit.png' alt='Edit'></a></td> 
                     <td><a><img src='../COVID-19%20Tracking%20Media/img/trash-bin.png' alt='Delete'></a></td>               
                </tr>
                ";

        }

        //Display a message if there is no records or results
    } else {
        echo "<h1>There is no records to display at this time</h1>";
        exit();
    }
    echo "</table>
            </div>";
    //Free the memory from the server
    $viewCurrentGuests->free_result();

    //Close the database
    $mysqli->close();

    ?>
</table>
<!-- Bootstrap core JS-->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Third party plugin JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<!-- Core theme JS-->
<script src="js/scripts.js"></script>
</body>
</html>
