<?php


function displayLoginNavBar(){
    if(session_status() === PHP_SESSION_NONE) {


echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../welcome.php'>Home </a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='../UI/services.php'>Services</a></li>
                <li class='nav-item'><a class='nav-link js-scroll-trigger' href='../User%20Login/userLogin.php'>Login</a></li>
            </ul>
        </div>
    </div>
    <br>
<br>
<br>
";



    } else if (session_status() === PHP_SESSION_ACTIVE) {


echo"<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";

    }
}



function displayUsersNavBar() {
if (isset($_POST['btn-user-register'])) {


    echo"<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
            <li class='nav-item'><a class='nav-link' href='../User/user.php'><img
                                src='../COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";


} else if (isset($_POST['btn-register'])) {

    echo"<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
            <li class='nav-item'><a class='nav-link' href='../Admin/adminUser.php'><img
                                src='../COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
}

}



function userNavBar()
{
    if (isset($_GET['userAccessId'])) {
        $id = $_GET['userAccessId'];
        echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?userAccessId=$id'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
    }
}


function adminNavbar(){
    if (isset($_GET['userAccessId'])) {
        $id = $_GET['userAccessId'];
        echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='../welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
          
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?userAccessId=$id'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
    }
}

function displayNavbar()
{
    if (isset($_SESSION['activeLogin'])) {
        $id = $_SESSION['SESS_ID'];
        echo "<nav class='navbar navbar-expand-lg navbar-light fixed-top py-3' id='mainNav'>
    <div class='container'>
        <a class='navbar-brand js-scroll-trigger' href='./welcome.php'>K & J Ministries</a>
        <button class='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'><span class='navbar-toggler-icon'></span></button>
        <div class='collapse navbar-collapse' id='navbarResponsive'>
            <ul class='navbar-nav ml-auto my-2 my-lg-0'>
           <li class='nav-item'><a class='nav-link' href='../Admin/adminUser.php?userAccessId=$id'><img 
            src='../COVID-19%20Tracking%20Media/img/home.png' alt='Home'> </a></li>
                <li class='nav-item'><a class='nav-link' href='../User%20Login/userLogin.php?userAccessId=$id'><img src='../COVID-19%20Tracking%20Media/img/exit.png' alt='Logout'> </a></li>
            </ul>
        </div>
    </div>
</nav>
    <br>
<br>
<br>";
    }
}




?>










