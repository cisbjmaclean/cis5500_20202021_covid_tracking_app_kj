<?php

session_start();

require_once("../Data Access Object/connectDAO.php");
require_once("../Admin/validateForms.php");
require_once("../Other/header.php");

confirmUserRegistration();


global $mysqli;
//Prevents user from accessing this page unless answered the Covid questions
if(!isset($_SERVER['HTTP_REFERER'])) {
    header("location: ../validateUser.php");
}

//NOTE: USE A SESSION TO CAPTURE USER INFO (NAME AND NUMBER) USING THE USER-ID FROM THE SESSION
if (isset( $_SESSION['SESS_ID'])) {
    $id = $_SESSION['SESS_ID'];

}


//********NOTE: USE THIS CODE TO DISPLAY THE USER FULLNAME AND PHONE NUMBER USING SESSIONS*****/////

$getUserDetails = $mysqli->prepare("SELECT useraccess.userAccessId, fullName, 
useraccessdetail.phoneNumber, useraccessdetail.userAccessId FROM useraccess INNER JOIN useraccessdetail
ON useraccess.userAccessId = useraccessdetail.userAccessId WHERE useraccess.userAccessId = '$id'");
$getUserDetails->execute();
$results = $getUserDetails->get_result();

//Display employee details from the database
if ($results->num_rows > 0) {
// output data of each row
    while ($row = $results->fetch_assoc()) {

        $userFullName = $row['fullName'];
        $userPhoneNumber = $row['phoneNumber'];
        $getUserDetails->free_result();
     //   $mysqli->close();
    }
}


// extract the GET variable isbn
if(isset($_GET['serviceId'])) {

//they have an isbn in the url
$service_Id = $_GET['serviceId'];



    $service_Id = $mysqli->real_escape_string($service_Id);

    // get the data for just the book we want to edit!
    $query = "SELECT * FROM service WHERE service.serviceId = '$service_Id'";
    $result = $mysqli->query($query);

    $num_results = $result->num_rows;

    if ($num_results == 0) {
        $message = "Service not found.";
    } else {
        $row = $result->fetch_assoc();
        $service_name = $row['service_name'];
        $service_description = $row['service_description'];
        $service_date = $row['service_date'];
        $service_time = $row['service_time'];
        $service_status = $row['service_status'];
        $service_quantity = $row['service_quantity'];

    }

    $result->free();
    $mysqli->close();
} else {
    //the id is not provided
    $message = "Sorry, no id provided.";
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reservations</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>

<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top py-3" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="../welcome.php">K & J Ministries</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0">
                <li class="nav-item"><a class="nav-link" href="user.php"><img src="../COVID-19%20Tracking%20Media/img/home.png" alt="Home"> </a></li>
                <li class="nav-item"><a class="nav-link" href="../User%20Login/userLogin.php"><img src="../COVID-19%20Tracking%20Media/img/exit.png" alt="Logout"> </a></li>
            </ul>
        </div>
    </div>
</nav>

<br>
<br>
<br>
<div id="container">
    <div class="album py-5 bg-light">
    <div class="form-row">
        <form action="" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="customer-heading">Service Registration</h2>
                        <div class="col-md-4">
                            <input type="text" id="congregant_name" class="form-control"
                                   name="congregant-fullname" value='<?php
                            echo $userFullName;
                            ?>'>
                            <!-----INSERT USER FULL NAME HERE----->

                        </div>
                        <br>

                        <br>
                        <div class="col-md-4">
                            <input type="tel" id="congregant_phone" class="form-control"
                                   name="congregant-number" value='<?php
                            echo $userPhoneNumber;
                            ?>'>
                            <!-----INSERT USER PHONE NUMBER HERE----->
                        </div>

                    <div class="col-md-4" id="service-div">
                        <label id="register-service-name" for="service-type">Current Services:</label>
                        <select name="current-service" id="service-type" class="custom-select">
                        <option><?php echo$service_name;?></option>

                        </select>
                    </div>

                    <div class="col-md-4">
                        <label id="register-service-date" for="service-date">Services Date:</label>
                    <select name="service-date" id="service-date" class="custom-select">
                    <option><?php echo $service_date; ?></option>

                    </select>

                </div>



                <div class="col-md-4">
                    <label id="register-service-time" for="service-qty">Service Times:</label>
                <select name="service-time" id="service-time" class="custom-select">
                <option><?php echo $service_time; ?></option>
                </select>

            </div>


                </div>
                </div>
                <br>

                <br>
                <button type="submit" id="btn-register-event" class="btn btn-outline-success" name="btn-user-register">Register</button>


                </div>

        </form>
    </div>
</div>

</body>
</html>

