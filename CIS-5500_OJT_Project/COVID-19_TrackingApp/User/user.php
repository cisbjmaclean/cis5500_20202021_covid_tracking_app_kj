<?php
//Starts session to ensure user is logged in
session_start();


require_once ("../Data Access Object/connectDAO.php");

require_once("../Admin/validateForms.php");
require_once("../Other/header.php");

//checkLoginForm();

//Gets current date and time
function getCurrentTime() {

    date_default_timezone_set('Canada/Atlantic');
    $month = date('M-d-Y');
    $time = date('h:i A');
    echo $month . " at " . $time;

}

//NOTE: USE A SESSION TO CAPTURE USER INFO (NAME AND NUMBER) USING THE USER-ID FROM THE SESSION
if (isset($_GET['userAccessId'])) {
    $id = $_GET['userAccessId'];
     $_SESSION['SESS_ID'] = $id;
     $_SESSION['activeLogin'] = true;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>K & J | User Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="../css/customStyles.css" rel="stylesheet">
</head>
<body>
<?php
echo userNavBar();
?>
</body>


        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">User</h1>

                <?php
                echo "You logged in ";
                echo getCurrentTime();
                ?>

                <div class="btn-toolbar mb-2 mb-md-0">

                </div>
            </div>

            <h2>Current Services</h2>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Service ID</th>
                        <th>Service Name</th>
                        <th>Service Description</th>
                        <th>Service Date</th>
                        <th>Service Time</th>
                        <th>Service Status</th>
                        <th>Service Quantity</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <?php

                    global $mysqli;
                    $viewCurrentServices = $mysqli->prepare("SELECT serviceId, service_name, service_description, service_date, service_time, service_status, service_quantity FROM service ORDER BY serviceId");
                    $viewCurrentServices->execute();
                    $results = $viewCurrentServices->get_result();

                    //Display employee details from the database
                    if ($results->num_rows > 0) {
                        // output data of each row
                        while ($row = $results->fetch_assoc()) {

                            $serviceID = $row["serviceId"];
                            $serviceName = $row["service_name"];
                            $serviceDesc = $row["service_description"];
                            $serviceDate = $row["service_date"];
                            $serviceTime = $row["service_time"];
                            $serviceStatus = $row["service_status"];
                            $serviceQty = $row["service_quantity"];


                            if ($serviceStatus == 'Unavailable') {
                                echo
                                "<tr><td>$serviceID</td>
                    <td>$serviceName</td>
                    <td>$serviceDesc</td>
                    <td>$serviceDate</td>
                    <td>$serviceTime</td>
                    <td>$serviceStatus</td>
                    <td>$serviceQty</td>
                    <td>Not Available</td>
               
              
                   
               
               </tr>
                ";

                            } else {

                                echo
                                    "<tr><td>$serviceID</td>
                    <td>$serviceName</td>
                    <td>$serviceDesc</td>
                    <td>$serviceDate</td>
                    <td>$serviceTime</td>
                    <td>$serviceStatus</td>
                    <td>$serviceQty</td>
                    <td><a href='../validateUser.php?serviceId=" . $serviceID . "' title='Register Service'><img src='../COVID-19%20Tracking%20Media/img/clipboard-with-pencil-.png' alt='Register'></a></td>
               
              
                   
               
               </tr>
                ";

                            }
                        }

                        //Display a message if there is no records or results
                    } else {
                        echo "<h1>There is no records to display at this time</h1>";
                        exit();
                    }
                    echo "</table>
            </div>";
                    //Free the memory from the server
                    $viewCurrentServices->free_result();

                    //Close the database
                    $mysqli->close();

                    ?>
            </div>
        </main>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="../assets/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
<script src="dashboard.js"></script></body>
</html>
