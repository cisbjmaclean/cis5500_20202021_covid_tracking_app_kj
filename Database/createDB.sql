DROP DATABASE if exists kj_ministries;
 CREATE DATABASE kj_ministries;
 USE kj_ministries;
 
 CREATE TABLE CodeType (codeTypeId int(3) COMMENT 'This is the primary key for code types',
   englishDescription varchar(100) NOT NULL COMMENT 'English description',
   frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
   createdDateTime datetime DEFAULT NULL,
   createdUserId varchar(20) DEFAULT NULL,
   updatedDateTime datetime DEFAULT NULL,
   updatedUserId varchar(20) DEFAULT NULL
 ) COMMENT 'This tables holds the code types that are available for the application';
 
 INSERT INTO CodeType (CodeTypeId, englishDescription, frenchDescription, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (1, 'User Types', 'User Types FR', sysdate(), '', CURRENT_TIMESTAMP, ''),
 (2, 'Status Types', 'Status Types FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, ''),
 (3, 'Organization details', 'Organization details FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, ''),
 (4, 'Guest Event Status', 'Guest Event Status FR', CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, '');
 
 CREATE TABLE CodeValue (
   codeTypeId int(3) NOT NULL COMMENT 'see code_type table',
   codeValueSequence int(3) NOT NULL,
   englishDescription varchar(100) NOT NULL COMMENT 'English description',
   englishDescriptionShort varchar(20) NOT NULL COMMENT 'English abbreviation for description',
   frenchDescription varchar(100) DEFAULT NULL COMMENT 'French description',
   frenchDescriptionShort varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
   sortOrder int(3) DEFAULT NULL COMMENT 'Sort order if applicable',
   createdDateTime datetime DEFAULT NULL,
   createdUserId varchar(20) DEFAULT NULL,
   updatedDateTime datetime DEFAULT NULL,
   updatedUserId varchar(20) DEFAULT NULL
 ) COMMENT='This will hold code values for the application.';
 
 INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (1, 1, 'Congregant', 'Congregant', 'CongregantFR', 'CongregantFR', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin');
 INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (1, 2, 'Admin', 'Admin', 'Admin', 'Admin', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin');
 
 INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (2, 1, 'Active', 'Active', 'Active', 'Active', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin'),
 (2, 2, 'Not Active', 'Not Active', 'Not Active', 'Not Active', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin');
 
 INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (3, 1, 'K&J Ministries','K&J', 'K&J MinistriesFR',  'K&J', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin'),
 (3, 2, 'Join us for praise and worship','Join Us', 'Join us for praise and worshipFR', 'Join usFR',  '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin');
 
 INSERT INTO CodeValue (codeTypeId, codeValueSequence, englishDescription, englishDescriptionShort, frenchDescription, frenchDescriptionShort, createdDateTime, createdUserId, updatedDateTime, updatedUserId) VALUES
 (4, 1, 'Registered','Registered', 'Registered',  'Registered', '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin'),
 (4, 2, 'Confirmed','Confirmed', 'ConfirmedFR', 'Join usFR',  '2015-10-25 18:44:37', 'Admin', '2015-10-25 18:44:37', 'Admin');
 
 
 CREATE TABLE UserAccess (
   userAccessId int NOT NULL AUTO_INCREMENT,
   username varchar(100) NOT NULL COMMENT 'Unique User name for app',
   pass_word varchar(128) NOT NULL,
   fullName varchar(128),
   userAccessStatusCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #2',
   userTypeCode int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
   createdDateTime datetime DEFAULT NULL COMMENT 'When User was created.',
     PRIMARY KEY(userAccessId)
 );
 
 
 INSERT INTO UserAccess (userAccessId, username, pass_word, fullName,userAccessStatusCode,userTypeCode, createdDateTime) VALUES
 (1, 'jjim@mail.com', '123', 'Jake Jim',1, 1, sysdate()),
 (2, 'kge@gmail.com', '123', 'Karen George',1, 1,sysdate()),
 (3, 'ktaylor@gmail.com', '123', 'Kevin Taylor',1,2,sysdate()),
 (4, 'akattijnr@hollandcollege.com', '123', 'Elikem Akatti Jnr',1,2,sysdate());
 
 CREATE TABLE UserAccessDetail (
   userAccessId int,
   phoneNumber  varchar(15) NOT NULL
 ) ;
 
 
 
 INSERT INTO UserAccessDetail (userAccessId, phoneNumber) VALUES
 (1,'9023141234'),
(2, '9023145678'),
(3,'9024587896'),
(4, '9023692587');
 
 
 CREATE TABLE  Service  (
    serviceId  int NOT NULL AUTO_INCREMENT,
    service_name  varchar(100) NOT NULL,
    service_description  varchar(240) NOT NULL,
    service_date  date NOT NULL,
    service_time  time NOT NULL DEFAULT current_timestamp(),
    service_status  varchar(20) DEFAULT NULL,
    service_quantity  int(10) DEFAULT NULL,
    PRIMARY KEY (serviceId)
 );
 
 
 INSERT INTO service (serviceId, service_name, service_description, service_date, service_time, service_status, service_quantity) VALUES
 (1, 'Sunday Worship Service', 'Sing along to your favourite songs and listen to a powerful message from our speaker during this service.', '2021-03-28', '09:00:00', 'Available ', 50),
 (2, 'Sunday Deliverance Service', 'Get saved and start your new journey with Christ ', '2021-04-04', '11:00:00', 'Available', 50),
 (3, 'Sunday Mass Service', 'Receive an inspiring message from our guest speakers', '2021-04-11', '11:00:00', 'Unavailable', 0),
 (4, 'Sunday Communion Service', 'Join us as we partake in the bread & wine ceremony to remember Christ\'s death', '2021-04-18', '09:00:00', 'Limited Capacity ', 25);
 
 
 CREATE TABLE ServiceGuest (
   id int(11) NOT NULL AUTO_INCREMENT,
   serviceId int(11) NOT NULL,
   serviceName varchar(100) NOT NULL,
   serviceDate date NOT NULL,
   serviceTime time NOT NULL,
   guestName varchar(100) NOT NULL,
   guestPhoneNumber varchar(15) NOT NULL,
   userAccessId int (3) NOT NULL,
   serviceGuestStatusType varchar(10) NOT NULL,
   PRIMARY KEY (id)
   );
 
 DELIMITER $$
 CREATE TRIGGER `update_service_qty` AFTER INSERT ON `serviceguest` FOR EACH ROW UPDATE service 
 JOIN serviceguest
 ON service.serviceId = serviceguest.serviceId
 SET service_quantity =service_quantity - 1 
 WHERE service.serviceId = NEW.serviceId
 $$
 DELIMITER ;