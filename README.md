

### What is this repository for? ###
*
*This repository will be a centralized location for all source code & documentation related to the K & J Ministries Covid-Tracking App 
*
*

### Deliverables ###

* Database Structure
* Web Application
* Desgin Documentation
* Testing Documentation
* User Guide

### Contribution guidelines ###

* Create database structure (tables and fields)
*Create screen mock-ups for system design
*Initial Version of Project
*
*
*

### Who do I talk to? ###

* Team Leads: Donnie McKinnon, BJ MacLean
* Team Members: Junior & Kevin